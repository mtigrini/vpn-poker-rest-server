const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
require("dotenv").config({
  path: `environments/.env.${process.env.NODE_ENV}`
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(morgan("dev"));
app.use(cors());

const users = require("./routes/users");
const lobbys = require("./routes/lobbys");
const tables = require("./routes/tables");
const chip_application = require("./routes/chip-application");
const join_lobby = require("./routes/join-lobby");

app.use("/api", users);
app.use("/api", lobbys);
app.use("/api", chip_application);
app.use("/api", join_lobby);
app.use("/api", tables);

module.exports = app;

const jwt = require("jsonwebtoken");
const auth = {
  checkJWT: function(req, res, next) {
    let token = req.headers["authorization"];
    if (token) {
      jwt.verify(token, process.env.jwtsecret, function(err, decoded) {
        if (err) {
          if (err["name"] == "TokenExpiredError") {
            res.status(403).json({
              success: false,
              message: "Token expired."
            });
          } else {
            res.status(401).json({
              success: false,
              message: "Failed to authenticate"
            });
          }
        } else {
          req.decoded = decoded;
          next();
        }
      });
    } else {
      res.status(403).json({
        success: false,
        message: "No token provided"
      });
    }
  }
};

module.exports = auth;

const mongoose = require("mongoose");
const app = require("./app");

require("dotenv").config({ path: `environments/.env.${process.env.NODE_ENV}` });

mongoose.connect(process.env.database, { useNewUrlParser: true }, err => {
  if (err) {
    console.log(err);
  } else {
    console.log("Connected to the database.");
  }
});

const port = process.env.PORT || 3030;
app.listen(port, err => {
  console.log("Magic happens on port " + port);
});

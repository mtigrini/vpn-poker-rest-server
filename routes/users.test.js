const request = require("supertest");
const mongoose = require("mongoose");
const UserModel = require("../models/user");
const nodemailer = require("nodemailer");
const jwt = require("jsonwebtoken");
const sinon = require("sinon");
const auth = require("../middlewares/check-jwt");
const ObjectId = require("mongoose").Types.ObjectId;
// You have to replace the middleware before you create the app!
sinon.stub(auth, "checkJWT").callsFake(async (req, res, next) => {
  req.decoded = {};
  req.decoded.user = {};
  req.decoded.user._id = new ObjectId();
  next();
});
const app = require("../app");

describe("User route endpoints", () => {
  beforeAll(async () => {
    await mongoose.connect(
      process.env.database,
      { useNewUrlParser: true },
      err => {
        if (err) {
          process.exit(1);
        }
      }
    );
  });

  afterAll(async () => {
    // Closing the connection to the database after all tests executed.
    await mongoose.connection.close();
  });

  describe("ENDPOINT /api/signup", () => {
    afterEach(async () => {
      await UserModel.deleteMany();
    });

    // TODO: Find a way to mock nodemailer and make another test.
    test("it should return 422 for bad input", async done => {
      request(app)
        .post(`/api/signup`)
        .send({
          username: "test",
          email: "test@gmail.com"
        })
        .then(response => {
          expect(response.statusCode).toBe(422);
          done();
        });
    });

    test("It should return a 500 because nodemailer failed", async done => {
      jest.spyOn(nodemailer, "createTransport").mockReturnValue({
        sendMail: jest.fn((message, callback) => {
          callback("Nodemailer error.");
        })
      });

      request(app)
        .post(`/api/signup`)
        .send({
          username: "test",
          email: "test@gmail.com",
          password: "test"
        })
        .then(response => {
          expect(response.statusCode).toBe(500);
          done();
        });
    });

    test("It should create an account", async done => {
      jest.spyOn(nodemailer, "createTransport").mockReturnValue({
        sendMail: jest.fn((message, callback) => {
          callback(null, "info");
        })
      });

      request(app)
        .post(`/api/signup`)
        .send({
          username: "test",
          email: "test@gmail.com",
          password: "test"
        })
        .then(response => {
          expect(response.statusCode).toBe(200);
          done();
        });
    });

    test("Should return 401 for duplicate username", async done => {
      // possible de mock une fonction appelee dans le fichier.
      jest.spyOn(nodemailer, "createTransport").mockReturnValue({
        sendMail: jest.fn((message, callback) => {
          callback(null, "info");
        })
      });

      await request(app)
        .post(`/api/signup`)
        .send({
          username: "test",
          email: "test@gmail.com",
          password: "test"
        });

      request(app)
        .post(`/api/signup`)
        .send({
          username: "test",
          email: "test2@gmail.com",
          password: "test2"
        })
        .then(response => {
          expect(response.statusCode).toBe(401);
          done();
        });
    });

    // TODO: Cover the user.save error.

    test("Should return 401 for duplicate email", async done => {
      jest.spyOn(nodemailer, "createTransport").mockReturnValue({
        sendMail: jest.fn((message, callback) => {
          callback(null, "info");
        })
      });

      await request(app)
        .post(`/api/signup`)
        .send({
          username: "test",
          email: "test@gmail.com",
          password: "test"
        });

      request(app)
        .post(`/api/signup`)
        .send({
          username: "test2",
          email: "test@gmail.com",
          password: "test2"
        })
        .then(response => {
          expect(response.statusCode).toBe(401);
          done();
        });
    });
  });

  describe("ENDPOINT /api/login", () => {
    afterEach(async () => {
      await UserModel.deleteMany();
    });

    test("Should return  422 because bad input", async done => {
      request(app)
        .post(`/api/login`)
        .send({
          username: "test"
        })
        .then(response => {
          expect(response.statusCode).toBe(422);
          done();
        });
    });

    // TODO: Cover the error 500 at UserModel.findOne();

    test("Should return 404", async done => {
      request(app)
        .post(`/api/login`)
        .send({
          usernameOrEmail: "test",
          password: "test"
        })
        .then(response => {
          expect(response.statusCode).toBe(404);
          done();
        });
    });

    test("Should return 401 because account is not confirmed", async done => {
      await request(app)
        .post(`/api/signup`)
        .send({
          username: "test",
          email: "test@gmail.com",
          password: "test"
        });

      request(app)
        .post("/api/login")
        .send({
          usernameOrEmail: "test",
          password: "test"
        })
        .then(response => {
          expect(response.statusCode).toBe(401);
          done();
        });
    });

    test("Should return 401 because bad password", async done => {
      await request(app)
        .post(`/api/signup`)
        .send({
          username: "test",
          email: "test@gmail.com",
          password: "test"
        });

      await UserModel.updateOne(
        { email: "test@gmail.com" },
        {
          $set: {
            isAccountConfirmed: true
          }
        }
      );

      request(app)
        .post("/api/login")
        .send({
          usernameOrEmail: "test",
          password: "2"
        })
        .then(response => {
          expect(response.statusCode).toBe(401);
          done();
        });
    });

    test("Should return 200", async done => {
      // Mocking the value of jwt
      jest.spyOn(jwt, "sign").mockReturnValue("123");
      await request(app)
        .post(`/api/signup`)
        .send({
          username: "test",
          email: "test@gmail.com",
          password: "test"
        });

      await UserModel.updateOne(
        { email: "test@gmail.com" },
        {
          $set: {
            isAccountConfirmed: true
          }
        }
      );

      request(app)
        .post("/api/login")
        .send({
          usernameOrEmail: "test",
          password: "test"
        })
        .then(response => {
          expect(response.statusCode).toBe(200);
          done();
        });
    });
  });

  describe("ENDPOINT /api/confirm-registration", () => {
    afterEach(async () => {
      await UserModel.deleteMany();
    });

    test("Should return 500", async done => {
      request(app)
        .post(`/api/confirm-registration/123`)
        .then(response => {
          expect(response.statusCode).toBe(404);
          done();
        });
    });

    test("It should return 409 because account already confirmed", async done => {
      await request(app)
        .post(`/api/signup`)
        .send({
          username: "test",
          email: "test@gmail.com",
          password: "test"
        });

      await UserModel.updateOne(
        { username: "test" },
        { $set: { isAccountConfirmed: true } }
      );

      const user = await UserModel.findOne({ username: "test" });

      request(app)
        .post(`/api/confirm-registration/${user.confirmationToken}`)
        .then(response => {
          expect(response.statusCode).toBe(409);
          done();
        });
    });

    test("It should return 200", async done => {
      await request(app)
        .post(`/api/signup`)
        .send({
          username: "test",
          email: "test@gmail.com",
          password: "test"
        });

      const user = await UserModel.findOne({ username: "test" });
      request(app)
        .post(`/api/confirm-registration/${user.confirmationToken}`)
        .then(response => {
          expect(response.statusCode).toBe(200);
          done();
        });
    });
  });

  describe("ENDPOINT /api/profile", () => {
    beforeAll(async () => {
      // Mocking the middleware. It is not tested here.
    });

    afterEach(async () => {
      await UserModel.deleteMany();
    });

    test("Should return 404", async done => {
      request(app)
        .get("/api/profile")
        .then(response => {
          expect(response.statusCode).toBe(404);
          done();
        });
    });
  });
});

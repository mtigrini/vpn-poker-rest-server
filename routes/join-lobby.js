const router = require("express").Router();
const LobbyModel = require("../models/lobby");
const UserModel = require("../models/user");
const JoinLobbyRequestModel = require("../models/join_lobby_request");
const JOIN_LOBBY_STATUS = require("../enums/join.lobby.status");
const auth = require("../middlewares/check-jwt");

router.post("/join-lobby/:lid", auth.checkJWT, (req, res, next) => {
  LobbyModel.findOne({ _id: req.params.lid }, (err, lobby) => {
    if (err) {
      res.status(500).json({
        success: false,
        message: "Error querying for lobby."
      });
      return;
    }

    if (!lobby) {
      res.status(404).json({
        success: false,
        message: "Lobby not found."
      });
      return;
    }

    // Checking if user is a lobby admin
    if (lobby.creator == req.decoded.user._id) {
      res.status(509).json({
        success: true,
        message: "Already an admin of the lobby."
      });
      return;
    }

    const member = lobby.members.find(m => m.uid == req.decoded.user._id);
    if (member) {
      res.status(509).json({
        success: false,
        message: "Already a member of the lobby."
      });
      return;
    }

    const removedMembers = lobby.removedMembers.find(
      m => m.uid == req.decoded.user._id
    );
    if (removedMembers) {
      res.status(401).json({
        success: false,
        message: "You were banned from this lobby."
      });
      return;
    }

    JoinLobbyRequestModel.findOne(
      { $and: [{ uid: req.decoded.user._id }, { lid: req.params.lid }] },
      (err, jlr) => {
        if (err) {
          res.status(500).json({
            success: false,
            message: "Error querying for join lobby request."
          });
          return;
        }

        if (jlr) {
          res.status(509).json({
            success: false,
            message: "Already asked to join lobby."
          });
          return;
        }

        let joinLobbyRequest = new JoinLobbyRequestModel();
        joinLobbyRequest.uid = req.decoded.user._id;
        joinLobbyRequest.username = req.decoded.user.username;
        joinLobbyRequest.lid = req.params.lid;
        joinLobbyRequest.save(function(err) {
          if (err) {
            res.status(500).json({
              success: false,
              message: "Failed to save join lobby request."
            });
            return;
          }

          res.json({
            success: true,
            message: "Successfully saved join lobby request."
          });
        });
      }
    );
  });
});

// TODO: Passer par jlrid au lieu du lid.
router.post(
  "/decide-lobby-join-request/:lid",
  auth.checkJWT,
  (req, res, next) => {
    if (
      !req.body.verdict ||
      (req.body.verdict != JOIN_LOBBY_STATUS.APPROVED &&
        req.body.verdict != JOIN_LOBBY_STATUS.DECLINED)
    ) {
      res.status(422).json({
        success: false,
        message: "Bad input. Bad verdict."
      });
      return;
    }

    if (!req.body.jlrid) {
      res.status(422).json({
        success: false,
        message: "Bad input. jlrid missing."
      });
      return;
    }

    LobbyModel.findOne(
      {
        _id: req.params.lid
      },
      (err, lobby) => {
        if (err) {
          res.status(500).json({
            success: false,
            message: "Error querying for lobby."
          });
          return;
        }

        if (!lobby) {
          res.status(404).json({
            success: false,
            message: "Lobby not found."
          });
          return;
        }

        if (lobby.creator != req.decoded.user._id) {
          res.status(401).json({
            success: false,
            message:
              "Unauthorized to do this action. You are not the lobby admin."
          });
          return;
        }

        JoinLobbyRequestModel.findOne({ _id: req.body.jlrid }, (err, jlr) => {
          if (err) {
            res.status(500).json({
              success: false,
              message: "Failed to query for join lobby request."
            });
            return;
          }

          if (!jlr) {
            res.status(404).json({
              success: false,
              message: "Join lobby request not found."
            });
            return;
          }

          if (jlr.status != JOIN_LOBBY_STATUS.PENDING) {
            res.status(409).json({
              success: false,
              message: "Join lobby request already processed."
            });
            return;
          }

          jlr.status = req.body.verdict;

          jlr.save(function(err) {
            if (err) {
              res.status(500).json({
                success: false,
                message: "Failed to save join lobby request."
              });
              return;
            }

            if (req.body.verdict == JOIN_LOBBY_STATUS.APPROVED) {
              lobby.members.push({
                uid: jlr.uid
              });
              lobby.save(function(err) {
                if (err) {
                  jlr.status = JOIN_LOBBY_STATUS.PENDING;
                  jrl.save(); // TODO: What if err happens here.
                  lobby.members = lobby.members.filter(m => m.uid != jlr.uid);
                  lobby.save();
                  res.status(500).json({
                    success: false,
                    message: "Failed to save lobby."
                  });
                  return;
                }

                UserModel.findOne({ _id: jlr.uid }, (err, user) => {
                  if (err) {
                    jlr.status = JOIN_LOBBY_STATUS.PENDING;
                    jrl.save(); // TODO: What if err happens here.
                    lobby.members = lobby.members.filter(m => m.uid != jlr.uid);
                    lobby.save();
                    res.status(500).json({
                      success: false,
                      message: "Failed to query for user"
                    });
                    return;
                  }

                  if (!user) {
                    jlr.status = JOIN_LOBBY_STATUS.PENDING;
                    jrl.save(); // TODO: What if err happens here.
                    lobby.members = lobby.members.filter(m => m.uid != jlr.uid);
                    lobby.save();
                    res.status(404).json({
                      success: false,
                      message: "User not found"
                    });
                    return;
                  }

                  user.lobbyChips.push({
                    lid: req.params.lid,
                    amount: 0
                  });

                  user.save(function(err) {
                    if (err) {
                      jlr.status = JOIN_LOBBY_STATUS.PENDING;
                      jrl.save(); // TODO: What if err happens here.
                      lobby.members = lobby.members.filter(
                        m => m.uid != jlr.uid
                      );
                      lobby.save();
                      res.status(500).json({
                        success: false,
                        message: "Error saving user."
                      });
                      return;
                    }

                    res.json({
                      success: true,
                      message: "Successfully treated join lobby request."
                    });
                  });
                });
              });
            } else {
              res.json({
                success: true,
                message: "Successfully treated join lobby request."
              });
            }
          });
        });
      }
    );
  }
);

router.get(
  "/lobby/:lid/join-lobby-requests",
  auth.checkJWT,
  (req, res, next) => {
    LobbyModel.findOne({ _id: req.params.lid }, (err, lobby) => {
      if (err) {
        res.status(500).json({
          success: false,
          message: "Error querying for lobby."
        });
        return;
      }

      if (!lobby) {
        res.status(404).json({
          success: false,
          message: "Lobby not found."
        });
        return;
      }

      if (lobby.creator != req.decoded.user._id) {
        res.status(403).json({
          success: false,
          message: "You are unauthorized to view this information."
        });
        return;
      }

      const statuses = req.query.status
        ? req.query.status.includes(",")
          ? req.query.status.split(",")
          : [req.query.status]
        : [];

      const params = statuses.length > 0 ? { status: { $in: statuses } } : {};
      JoinLobbyRequestModel.find(params, (err, joinLobbyRequests) => {
        if (err) {
          res.status(500).json({
            success: false,
            message: "Failed to query for join lobby requests."
          });
          return;
        }

        res.json({
          success: true,
          message: "Successfully retrieved lobby join requests.",
          joinLobbyRequests: joinLobbyRequests
        });
      });
    });
  }
);

module.exports = router;

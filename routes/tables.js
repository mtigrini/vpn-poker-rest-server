const router = require("express").Router();
const TableModel = require("../models/table").tableModel;
const LobbyModel = require("../models/lobby");
const GameLogModel = require("../models/game_logs");
const UserModel = require("../models/user");
const TransactionModel = require("../models/transaction");
const auth = require("../middlewares/check-jwt");
const GAME_MODE = require("../enums/game.settings");
const GAME_TYPE = require("../enums/game.type");

// TODO: lobby/:lid/tables
router
  .route("/tables/:lid")
  .get(auth.checkJWT, (req, res, next) => {
    LobbyModel.findOne({ _id: req.params.lid }, (err, lobby) => {
      if (err) {
        res.status(500).json({
          success: false,
          message: "Failed to retrieve lobby."
        });
        return;
      }

      if (!lobby) {
        res.status(404).json({
          success: false,
          message: "Lobby not found."
        });
        return;
      }

      if (lobby.creator != req.decoded.user._id) {
        let isMember = false;
        for (let member of lobby.members) {
          if (member.uid == req.decoded.user._id) {
            isMember = true;
            break;
          }
        }
        if (!isMember) {
          res.status(401).json({
            success: false,
            message: "Not authorized to view this lobby's tables."
          });
          return;
        }
      }

      TableModel.find({ lid: req.params.lid }, (err, tables) => {
        if (err) {
          res.status(500).json({
            success: false,
            message: "Failed to retrieve tables."
          });
          return;
        }

        res.json({
          success: true,
          message: "Successfully retrieved tables.",
          tables: tables
        });
      });
    });
  })
  .post(auth.checkJWT, async (req, res, next) => {
    const {
      smallBlind,
      bigBlind,
      minBuyIn,
      maxBuyIn,
      minPlayers,
      maxPlayers,
      gameMode,
      rake,
      gameType
    } = req.body;
    if (
      smallBlind == undefined ||
      bigBlind == undefined ||
      minBuyIn == undefined ||
      maxBuyIn == undefined ||
      minPlayers == undefined ||
      maxPlayers == undefined ||
      gameMode == undefined ||
      rake == undefined ||
      gameType == undefined
    ) {
      res.status(422).json({
        success: false,
        message: "Bad input."
      });
      return;
    }

    if (smallBlind < 0 || bigBlind < 0 || smallBlind >= bigBlind) {
      res.status(422).json({
        success: false,
        message: "Invalid small blind or big blind."
      });
      return;
    }

    if (minBuyIn < 0 || maxBuyIn < 0 || minBuyIn >= maxBuyIn) {
      res.status(422).json({
        success: false,
        message: "Invalid minimum buy in or maximum buy in."
      });
      return;
    }

    if (
      minPlayers < 2 ||
      minPlayers > 9 ||
      maxPlayers < 2 ||
      maxPlayers > 9 ||
      minPlayers > maxPlayers
    ) {
      res.status(422).json({
        success: false,
        message: "Invalid minimum players or maximum players."
      });
      return;
    }

    if (gameMode != GAME_MODE.FAST && gameMode != GAME_MODE.NORMAL) {
      res.status(422).json({
        success: false,
        message: "Invalid game mode."
      });
      return;
    }

    if (rake < 0 || rake > 30) {
      res.status(422).json({
        success: false,
        message: "Invalide rake."
      });
      return;
    }

    if (gameType != GAME_TYPE.OMAHA && gameType != GAME_TYPE.TXSHLDM) {
      res.status(422).json({
        success: false,
        message: "Invalide game type."
      });
      return;
    }

    LobbyModel.findOne({ _id: req.params.lid }, async (err, lobby) => {
      if (err) {
        res.status(500).json({
          success: false,
          message: "Error querying for lobby."
        });
        return;
      }

      if (!lobby) {
        res.status(404).json({
          success: false,
          message: "Lobby not found."
        });
        return;
      }

      let table = new TableModel();
      table.lid = req.params.lid;
      table.smallBlind = smallBlind;
      table.bigBlind = bigBlind;
      table.minBuyIn = minBuyIn;
      table.maxBuyIn = maxBuyIn;
      table.minPlayers = minPlayers;
      table.maxPlayers = maxPlayers;
      table.gameMode = gameMode;
      table.rake = rake;
      table.gameType = gameType;
      table.creator = req.decoded.user._id;

      // Check if user is admin of the lobby
      if (lobby.creator != req.decoded.user._id) {
        res.status(401).json({
          success: false,
          message: "You are not authorized to create a lobby."
        });
        return;
      }

      TableModel.create(table, (err, createdTable) => {
        if (err) {
          res.status(500).json({
            success: false
          });
          return;
        }

        res.json({
          success: true,
          message: "Successfully created lobby."
        });
      });
    });
  });

router
  .route("/lobbys/:lid/tables/:tid")
  .get(auth.checkJWT, (req, res, next) => {
    LobbyModel.findOne({_id: req.params.lid}, (err, lobby) => {
      if (err) {
        res.status(500).json({
          success: false,
          message: "Failed to query for table."
        });
        return;
      }

      if(!lobby) {
        res.status(404).json({
          succcess: false,
          message: "Lobby does not exist."
        });
        return;
      }

      TableModel.findOne({ _id: req.params.tid }, (err, table) => {
        if (err) {
          res.status(500).json({
            success: false,
            message: "Failed to query for table."
          });
          return;
        }
  
        if (!table) {
          res.status(404).json({
            success: false,
            message: "Table not found."
          });
          return;
        }
  
        if(table.lid != req.params.lid) {
          res.status(422).json({
            succcess: false,
            message: "Invalid table."
          });
          return;
        }

        res.json({
          success: true,
          message: "Successfully fetched table.",
          table: table
        });
        return;
      });
    });
  })
  .put(auth.checkJWT, (req, res, next) => {
    TableModel.findOne({ _id: req.params.tid }, (err, table) => {
      if (err) {
        res.status(500).json({
          success: false,
          message: "Failed to query for table."
        });
        return;
      }

      if (!table) {
        res.status(404).json({
          success: false,
          message: "Table not found."
        });
        return;
      }

      // TODO:
      // res.json({
      //   success: true,
      //   message: "Successfully updated table."
      // });
      // return;
    });
  });

// TODO: lobby/:lid/tables/:tid/cashin
router.route("/table/:tid/cashin").post(auth.checkJWT, (req, res, next) => {
  if (!req.body.secret || typeof req.body.secret != "string") {
    res.status(500).json({
      success: false,
      message: "Unauthorized to to this action."
    });
    return;
  }

  TableModel.findOne({ _id: req.params.tid }, (err, table) => {
    if (err) {
      res.status(500).json({
        success: false,
        message: "Failed to query for table."
      });
      return;
    }

    if (!table) {
      res.status(404).json({
        success: false,
        message: "Table not found."
      });
      return;
    }

    const secret =
      table._id
        .toString()
        .split("")
        .reverse()
        .join("") + "secret";

    if (secret != req.body.secret) {
      res.status(500).json({
        success: false,
        message: "Unauthorized to to this action."
      });
      return;
    }

    LobbyModel.findOne({ _id: table.lid }, (err, lobby) => {
      if (err) {
        res.status(500).json({
          success: false,
          message: "Error querying for lobby."
        });
        return;
      }

      if (!lobby) {
        res.status(404).json({
          success: false,
          message: "Lobby not found."
        });
        return;
      }

      UserModel.findOne({ _id: req.decoded.user._id }, async (err, user) => {
        {
          if (err) {
            res.status(500).json({
              success: false,
              message: "Error querying for user."
            });
            return;
          }

          if (!user) {
            res.status(404).json({
              success: false,
              message: "Error querying for user."
            });
            return;
          }

          if (
            lobby.creator != req.decoded.user._id &&
            !lobby.members.find(m => m.uid == req.decoded.user._id)
          ) {
            res.status(401).json({
              success: false,
              message: "Not a member of the lobby."
            });
            return;
          }

          UserModel.findOne(
            { _id: req.decoded.user._id },
            async (err, user) => {
              {
                if (err) {
                  res.status(500).json({
                    success: false,
                    message: "Error querying for user."
                  });
                  return;
                }

                if (!user) {
                  res.status(404).json({
                    success: false,
                    message: "Error querying for user."
                  });
                  return;
                }

                var chips = user.lobbyChips.find(c => c.lid == table.lid);
                var chipsIndex = user.lobbyChips.indexOf(chips);

                if (!chips || !chips.amount || chips.amount < req.body.amount) {
                  res.status(500).json({
                    success: false,
                    message: "Not enough chips."
                  });
                  return;
                }

                if (req.body.amount < table.minBuyIn) {
                  res.status(500).json({
                    success: false,
                    message:
                      "Buy in is below the minimum required for this table."
                  });
                  return;
                }

                chips.amount -= req.body.amount;
                user.lobbyChips[chipsIndex] = chips;

                user.save(function(err) {
                  if (err) {
                    res.status(500).json({
                      success: false,
                      message: "Error occured. Could not set your player chips."
                    });
                    return;
                  }

                  let transaction = new TransactionModel();
                  transaction.uid = req.decoded.user._id;
                  transaction.tid = table._id;
                  transaction.lid = table.lid;
                  transaction.cashin = {
                    amount: req.body.amount
                  };

                  transaction.save(async function(err, savedTransaction) {
                    if (err) {
                      chips.amount += req.body.amount;
                      user.lobbyChips[chipsIndex] = chips;
                      await user.save();
                      res.status(500).json({
                        success: false,
                        message: "Failed to save transaction."
                      });
                      return;
                    }

                    res.json({
                      success: true,
                      message: "Successfully joined table.",
                      gameSessionId: savedTransaction._id
                    });
                  });
                });
              }
            }
          );
        }
      });
    });
  });
});

// TODO: lobby/:lid/tables/:tid/cashout/:gsid
router.route("/table/:tid/cashout/:gsid").post((req, res, next) => {
  if (!req.body.secret || typeof req.body.secret != "string") {
    res.status(500).json({
      success: false,
      message: "Unauthorized to do this action."
    });
    return;
  }

  TableModel.findOne({ _id: req.params.tid }, (err, table) => {
    if (err) {
      res.status(500).json({
        success: false,
        message: "Error querying table."
      });
      return;
    }

    if (!table) {
      res.status(404).json({
        success: false,
        message: "Table not found."
      });
      return;
    }

    if (process.env.SECRET != req.body.secret) {
      res.status(401).json({
        success: false,
        message: "Unauthorized to to this action."
      });
      return;
    }

    LobbyModel.findOne({ _id: table.lid }, (err, lobby) => {
      if (err) {
        res.status(500).json({
          success: false,
          message: "Error querying for lobby."
        });
        return;
      }

      if (!lobby) {
        res.status(404).json({
          success: false,
          message: "Lobby not found."
        });
        return;
      }

      UserModel.findOne(
        { _id: req.body.uid },
        { _id: 1, username: 1, lobbyChips: 1 },
        (err, user) => {
          if (err) {
            res.status(500).json({
              success: false,
              message: "Error querying for user."
            });
            return;
          }

          if (!user) {
            res.status(404).json({
              success: false,
              message: "User nor found."
            });
            return;
          }

          if (
            lobby.creator != req.body.uid &&
            !lobby.members.find(m => m.uid == req.body.uid)
          ) {
            res.status(401).json({
              success: false,
              message: "Not a member of the lobby."
            });
            return;
          }

          TransactionModel.findOne(
            { _id: req.params.gsid },
            (err, transaction) => {
              if (err) {
                res.status(500).json({
                  success: false,
                  message: "Failed to query for transaction."
                });
                return;
              }

              if (!transaction) {
                res.status(404).json({
                  success: false,
                  message: "Transaction not found."
                });
                return;
              }

              if (transaction.cashout) {
                res.status(500).json({
                  succcess: false,
                  message: "Already cashed out for this transaction."
                });
                return;
              }

              var chips = user.lobbyChips.find(c => c.lid == lobby._id);
              var chipsIndex = user.lobbyChips.indexOf(chips);
              chips.amount += Math.round(req.body.amount);
              user.lobbyChips[chipsIndex] = chips;

              user.save(async function(err) {
                if (err) {
                  res.status(500).json({
                    succcess: false,
                    message: "Could not update user."
                  });
                  return;
                }

                transaction.cashout = {
                  amount: req.body.amount
                };

                transaction.save(async function(err) {
                  if (err) {
                    res.status(500).json({
                      success: false,
                      message: "Error. Could not cash out."
                    });
                    return;
                  }
                  res.json({
                    success: true,
                    message: "Successfully updated player info."
                  });
                });
              });
            }
          );
        }
      );
    });
  });
});

// TODO: POST lobby/:lid/tables/:tid/kickout
router
  .route("/kick/:uid/from-table/:tid")
  .get(auth.checkJWT, (req, res, next) => {
    TableModel.findOne({ _id: req.params.tid }, (err, table) => {
      if (err) {
        res.status(500).json({
          success: false,
          message: "Failed to query for table."
        });
        return;
      }

      if (!table) {
        res.status(404).json({
          success: false,
          message: "Table not found."
        });
        return;
      }

      LobbyModel.findOne({ _id: table.lid }, (err, lobby) => {
        if (err) {
          res.status(500).json({
            success: false,
            message: "Failed to query for lobby."
          });
          return;
        }

        if (!lobby) {
          res.status(404).json({
            success: false,
            message: "Lobby not found."
          });
          return;
        }

        if (lobby.creator != req.decoded.user._id) {
          res.status(401).json({
            success: true,
            message: "Unauthorized to do such action."
          });
          return;
        }

        // TOOD: Maybe save this in the database that the user got kicked out.
        res.json({
          success: true,
          message: "Authorized to do an action like that.",
          lobby: lobby,
          table: table
        });
      });
    });
  });

// TODO: lobby/:lid/tables/:tid/lock
router.route("/lock-table/:tid").post(auth.checkJWT, (req, res, next) => {
  TableModel.findOne({ _id: req.params.tid }, (err, table) => {
    if (err) {
      res.status(500).json({
        success: false,
        message: "Error querying for table."
      });
      return;
    }

    if (!table) {
      res.status(404).json({
        success: false,
        message: "Table not found."
      });
      return;
    }

    LobbyModel.findOne({ _id: table._id }, (err, lobby) => {
      if (err) {
        res.status(500).json({
          success: false,
          message: "Error querying for lobby."
        });
        return;
      }

      if (!lobby) {
        res.status(404).json({
          success: false,
          message: "Lobby not found,"
        });
        return;
      }

      if (lobby.creator != req.decoded.user._id) {
        res.status(401),
          json({
            success: false,
            message: "Unauthorized to perform such action."
          });
        return;
      }

      table.isLocked = req.body.isLocked;

      table.save(err => {
        if (err) {
          res.status(500).json({
            success: false,
            message: "Error occured locking table."
          });
          return;
        }

        res.json({
          success: true,
          message: "Successfully locked table."
        });
      });
    });
  });
}); 

// TODO: lobby/:lid/tables/:tid/past-rounds
router
  .route("/table/:tid/past-rounds")
  .get(auth.checkJWT, (req, res, next) => {
    TableModel.findOne({ _id: req.params.tid }, async (err, table) => {
      if (err) {
        res
          .status(500)
          .json({ success: false, message: "Error querying for table" });
        return;
      }

      if (!table) {
        res.status(404).json({
          success: false,
          message: "No table found."
        });
        return;
      }

      LobbyModel.findOne({ _id: table.lid }, async (err, lobby) => {
        if (err) {
          res.status(500).json({
            success: false,
            message: "Error querying for lobby."
          });
          return;
        }

        if (!lobby) {
          res.status(404).json({
            success: false,
            message: "Lobby not found."
          });
          return;
        }

        if (lobby.creator != req.decoded.user._id) {
          res.status(401).json({
            success: false,
            message: "Unauthorized to perform such action."
          });
          return;
        }

        GameLogModel.find({ lid: lobby._id })
          .sort({ start: -1 })
          .skip(
            info.page
              ? info.page * info.numberOfElements - info.numberOfElements
              : 0
          )
          .limit(info.numberOfElements)
          .then(async (err, gameLogs) => {
            if (err) {
              res.status(500).json({
                success: true,
                message: "Error querying for game logs."
              });
              return;
            }

            res.json({
              success: true,
              message: "Successfully locked lobby.",
              rounds: results,
              numberOfPages: Math.floor(
                (await GameLogModel.find({ lid: lobby._id }).count()) /
                  req.body.numberOfElements
              )
            });
          });
      });
    });
  })
  .post((req, res, next) => {
    // TODO: Valider que la communication vient du bon serveur.
    req.body.timeZone = "America/New_York"; // setting a default value just to be sure
    const {
      tid,
      lid,
      actionLogs,
      gems, // FIXME: Is this it?
      board,
      start,
      seats,
      timeZone,
      end
    } = req.body;

    let gameLog = new GameLogModel();
    gameLog.tid = tid;
    gameLog.lid = lid;
    gameLog.actionLogs = actionLogs;
    gameLog.gems = gems; // SETTING TO 0 SO THAT IF WE RESEND THIS, IT WON'T SAVE THE THINGS.
    gameLog.board = board;
    gameLog.start = start;
    gameLog.end = end;
    gameLog.seats = seats;
    gameLog.timeZone = timeZone; // La timezone par default

    // TODO: Input validation
    TableModel.findOne({ _id: req.params.tid }, async (err, table) => {
      if (err) {
        res.status(500).json({
          success: false,
          message: "Error querying for table",
          gameLog: gameLog
        });
        return;
      }

      if (!table) {
        res.status(404).json({
          success: false,
          message: "No table found.",
          gameLog: gameLog
        });
        return;
      }

      LobbyModel.findOne({ _id: table.lid }, async (err, lobby) => {
        if (err) {
          res.status(500).json({
            success: false,
            message: "Error querying for lobby.",
            gameLog: gameLog
          });
          return;
        }

        if (!lobby) {
          res.status(404).json({
            success: false,
            message: "Lobby not found.",
            gameLog: gameLog
          });
          return;
        }

        lobby.gems += gems;

        lobby.save(err => {
          if (err) {
            res.status(500).json({
              success: false,
              mssage: "Failed to save gems to lobby.",
              gameLog: gameLog
            });
            return;
          }

          GameLogModel.create(gameLog, (err, createdGameLog) => {
            if (err) {
              gameLog.gems = 0; // Because gems have already been saved...
              res.status(500).json({
                success: false,
                message: "Error saving game log.",
                gameLog: gameLog
              });
              return;
            }

            res.json({
              success: false,
              message: "Successfully saved game log."
            });
          });
        });
      });
    });
  });

module.exports = router;

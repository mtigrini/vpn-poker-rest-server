const router = require("express").Router();
const LobbyModel = require("../models/lobby");
const TableModel = require("../models/table");
const UserModel = require("../models/user");
const GameLogModel = require("../models/game_logs");
const MAX_NUMBER_OF_LOBBYS_ALLOWED = require("../config").lobbys
  .maxAmountAllowed;
const LOBBY_PERMISSIONS = require("../enums/lobby.permissions");
const auth = require("../middlewares/check-jwt");

router.get("/lobbys", auth.checkJWT, (req, res, next) => {
  LobbyModel.find(
    {
      $or: [
        { creator: req.decoded.user._id },
        { "members.uid": req.decoded.user._id }
      ]
    },
    { _id: 1, name: 1, creator: 1, isLocked: 1, members: 1 },
    (err, lobbys) => {
      if (err) {
        res.status(500).json({
          success: false,
          message: "Failed to retrieve lobbys."
        });
        return;
      }

      const sentLobbys = [];
      for (let lobby of lobbys) {
        sentLobbys.push({
          _id: lobby._id,
          name: lobby.name,
          creator: lobby.creator,
          isLocked: lobby.isLocked,
          members: lobby.members.length
        });
      }

      res.json({
        success: true,
        message: "Successfully retrieved lobbys.",
        lobbys: sentLobbys
      });
    }
  );
});

router.post("/lobby", auth.checkJWT, async (req, res, next) => {
  if (!req.body.name) {
    res.status(422).json({
      success: false,
      message: "Bad input."
    });
    return;
  }

  const numberOfCreatedLobbys = await LobbyModel.find({
    creator: req.decoded.user._id
  }).count();

  if (numberOfCreatedLobbys >= MAX_NUMBER_OF_LOBBYS_ALLOWED) {
    res.status(401).json({
      code: 401,
      success: false,
      message: "You are not authorized to create more lobbys."
    });
    return;
  }

  let lobby = new LobbyModel();
  lobby.name = req.body.name;
  lobby.creator = req.decoded.user._id;

  LobbyModel.create(lobby, (err, createdLobby) => {
    if (err) {
      res.status(500).json({
        success: false,
        message: "Error trying to create lobby."
      });
      return;
    }

    res.json({
      success: true,
      message: "Successfully created lobby!",
      lobby: createdLobby
    });
  });
});

router.get("/lobby/:lid/creator", async (req, res, next) => {
  LobbyModel.findOne({_id: req.params.lid}, (err, lobby) => {
    if (err) {
      res.status(500).json({
        success: true,
        message: "Faied to retrieve lobbys."
      });
      return;
    }

    if (!lobby) {
      res.status(404).json({
        success: false,
        message: "No lobby found."
      });
      return;
    }
    console.log("LOBBY CREATOR", lobby.creator);
    UserModel.findOne({_id: lobby.creator}, {_id: 1, username: 1, avatar: 1, created: 1}, (err, user) => {
      if(err) {
        console.log("ERR");
        res.status(500).json({
          success: false,
          message: "Faied to retrieve lobby's admin."
        });
        return;
      }

      if(!user) {
        res.status(404).json({
          success: false,
          message: "Lobby admin not found."
        });
        return;
      }

      res.json({
        success: true,
        message: "Successfully retrieved lobby admin",
        user: user
      });
    })
  });
});

router.get("/lobby/:id", auth.checkJWT, (req, res, next) => {
  LobbyModel.findOne(
    {
      _id: req.params.id
    },
    (err, lobby) => {
      if (err) {
        res.status(500).json({
          success: true,
          message: "Faied to retrieve lobbys."
        });
        return;
      }

      if (!lobby) {
        res.status(404).json({
          success: false,
          message: "No lobby found."
        });
        return;
      }

      if (lobby.creator == req.decoded.user._id) {
        res.json({
          success: true,
          message: "Successfully retrieved lobby.",
          lobby: {
            _id: lobby._id,
            name: lobby.name,
            created: lobby.created,
            creator: lobby.creator,
            members: lobby.members,
            removedMembers: lobby.removedMembers,
            chipApplications: lobby.chipApplications,
            joinLobbyRequests: lobby.joinLobbyRequests,
            permissions: LOBBY_PERMISSIONS.OWNER,
            isLocked: lobby.isLocked
          }
        });
      } else if (lobby.members.find(m => m.uid == req.decoded.user._id)) {
        res.json({
          code: 200,
          success: true,
          message: "Successfully retrieved lobby.",
          lobby: {
            _id: lobby._id,
            name: lobby.name,
            members: lobby.members,
            creator: lobby.creator,
            created: lobby.created,
            permissions: LOBBY_PERMISSIONS.MEMBER,
            isLocked: lobby.isLocked
          }
        });
      } else {
        res.status(401).json({
          success: false,
          message: "You are not apart of this lobby."
        });
      }
    }
  );
});

router.post("/lobby/:lid/kickout/:uid", auth.checkJWT, (req, res, next) => {
  UserModel.findOne({ _id: req.params.uid }, (err, user) => {
    if (err) {
      res.status(500).json({
        success: false,
        message: "Error querying for user."
      });
      return;
    }

    if (!user) {
      res.status(404).json({
        success: false,
        message: "User not found."
      });
      return;
    }

    LobbyModel.findOne({ _id: req.params.lid }, (err, lobby) => {
      if (err) {
        res.status(500).json({
          success: false,
          message: "Error querying for lobby."
        });
        return;
      }

      if (!lobby) {
        res.status(404).json({
          success: false,
          message: "Lobby not found."
        });
        return;
      }

      if (lobby.creator != req.decoded.user._id) {
        res.status(401).json({
          success: false,
          message: "Action unauthorized. You are not the lobby admin."
        });
        return;
      }

      let member = lobby.members.find(m => m.uid == req.params.uid);

      if (!member) {
        res.status(404).json({
          success: true,
          message: "The was not found."
        });
        return;
      }

      lobby.removedMembers.push(member);
      lobby.members = lobby.members.filter(m => m.uid != member.uid);

      lobby.save((err, updatedLobby) => {
        if (err) {
          res.status(500).json({
            success: false,
            message: "Error updating lobby. Could not kick out user."
          });
          return;
        }

        res.json({
          success: true,
          message: "Successfully kicked out user from lobby."
        });
      });
    });
  });
});

router.post(
  "/lobby/:lid/reinstate/:uid",
  auth.checkJWT,
  (req, res, next) => {
    UserModel.findOne({ _id: req.params.uid }, (err, user) => {
      if (err) {
        res.status(500).json({
          success: false,
          message: "Error querying for user."
        });
        return;
      }

      if (!user) {
        res.status(404).json({
          success: false,
          message: "User not found."
        });
        return;
      }

      LobbyModel.findOne({ _id: req.params.lid }, (err, lobby) => {
        if (err) {
          res.status(500).json({
            success: false,
            message: "Error querying for lobby."
          });
          return;
        }

        if (!lobby) {
          res.status(404).json({
            success: false,
            message: "Lobby not found."
          });
          return;
        }

        if (lobby.creator != req.decoded.user._id) {
          res.status(401).json({
            success: false,
            message: "Action unauthorized. You are not the lobby admin."
          });
          return;
        }

        let member = lobby.removedMembers.find(m => m.uid == req.params.uid);
        if (!member) {
          res.status(404).json({
            success: true,
            message: "The user was not found."
          });
          return;
        }

        lobby.members.push(member);
        lobby.removedMembers = lobby.removedMembers.filter(
          m => m.uid != member.uid
        );

        lobby.save((err, updatedLobby) => {
          if (err) {
            res.status(500).json({
              success: false,
              message: "Error updating lobby. Could not kick out user."
            });
            return;
          }

          res.json({
            success: true,
            message: "Successfully kicked out user from lobby."
          });
        });
      });
    });
  }
);

router.get("/lobby/:lid/members", auth.checkJWT, (req, res, next) => {
  LobbyModel.findOne({ _id: req.params.lid }, (err, lobby) => {
    if (err) {
      res.status(500).json({
        success: false,
        message: "Error querying for lobby."
      });
      return;
    }

    if (!lobby) {
      res.status(404).json({
        success: false,
        message: "Lobby not found."
      });
      return;
    }

    if (
      lobby.creator != req.decoded.user._id &&
      !lobby.members.find(member => member.uid == req.decoded.user._id)
    ) {
      res.status(401).json({
        success: false,
        message: "You are unauthorized in this lobby."
      });
      return;
    }

    let ids = [];
    for (let m of lobby.members) {
      ids.push(m.uid);
    }

    UserModel.find(
      { _id: { $in: ids } },
      { _id: 1, username: 1, avatar: 1, lobbyChips: 1 },
      (err, users) => {
        if (err) {
          res.status(500).json({
            success: false,
            message: "Error querying for members"
          });
          return;
        }

        let members = [];
        for (let usr of users) {
          members.push({
            _id: usr._id,
            username: usr.username,
            avatar: usr.avatar,
            lobbyChips: [usr.lobbyChips.find(l => l.lid == req.params.lid)]
          });
        }

        res.json({
          success: true,
          message: "Successfully retrieved online members of lobby.",
          members: members
        });
      }
    );
  });
});

router.get("/lobby/:lid/members/removed", auth.checkJWT, (req, res, next) => {
  LobbyModel.findOne({ _id: req.params.lid }, (err, lobby) => {
    if (err) {
      res.status(500).json({
        success: false,
        message: "Error querying for lobby."
      });
      return;
    }

    if (!lobby) {
      res.status(404).json({
        success: false,
        message: "Lobby not found."
      });
      return;
    }

    if (
      lobby.creator != req.decoded.user._id
    ) {
      res.status(401).json({
        success: false,
        message: "You are unauthorized to do this action."
      });
      return;
    }

    let ids = [];
    for (let m of lobby.removedMembers) {
      ids.push(m.uid);
    }

    UserModel.find(
      { _id: { $in: ids } },
      { _id: 1, username: 1, avatar: 1, lobbyChips: 1 },
      (err, users) => {
        if (err) {
          res.status(500).json({
            success: false,
            message: "Error querying for members"
          });
          return;
        }

        let members = [];
        for (let usr of users) {
          members.push({
            _id: usr._id,
            username: usr.username,
            avatar: usr.avatar,
            lobbyChips: [usr.lobbyChips.find(l => l.lid == req.params.lid)]
          });
        }

        res.json({
          success: true,
          message: "Successfully retrieved online members of lobby.",
          members: members
        });
      }
    );
  });
});

router.get("/lobby/:lid/member/:uid", auth.checkJWT, (req, res, next) => {
  LobbyModel.findOne({ _id: req.params.lid }, (err, lobby) => {
    if (err) {
      res.status(500).json({
        success: false,
        message: "Error querying for lobby."
      });
      return;
    }

    if (!lobby) {
      res.status(404).json({
        success: false,
        message: "Lobby not found."
      });
      return;
    }

    if (lobby.creator != req.decoded.user._id) {
      res.status(401).json({
        success: false,
        message: "You are unauthorized to get this information."
      });
      return;
    }

    let member = lobby.members.find(m => m.uid == req.params.uid);
    if (!member) {
      res.status(404).json({
        success: false,
        message: "User not apart of lobby."
      });
      return;
    }

    UserModel.findOne(
      { _id: req.params.uid },
      { _id: 1, username: 1, avatar: 1, lobbyChips: 1 },
      (err, user) => {
        if (err) {
          res.status(500).json({
            success: false,
            message: "Error querying for lobby."
          });
          return;
        }

        if (!user) {
          res.status(404).json({
            success: false,
            message: "User not found."
          });
          return;
        }

        const member = {
          _id: user._id,
          username: user.username,
          avatar: user.avatar,
          lobbyChips: [user.lobbyChips.find(l => l.lid == req.params.lid)]
        };

        res.json({
          success: true,
          message: "Successfully retrieved lobby member.",
          member: member
        });
      }
    );
  });
});

router
  .route("/lobby/:lid/past-rounds")
  .get(auth.checkJWT, (req, res, next) => {
    LobbyModel.findOne({ _id: req.params.lid }, async (err, lobby) => {
      if (err) {
        res
          .status(500)
          .json({ success: false, message: "Error querying for lobby" });
        return;
      }

      if (!lobby) {
        res.status(404).json({
          success: false,
          message: "No lobby found."
        });
        return;
      }

      if (lobby.creator != decoded.user._id) {
        res.status(401).json({
          success: false,
          message: "You are not the lobby admin."
        });
        return;
      }

      const results = await GameLogModel.find({ lid: req.params.lid })
        .sort({ start: -1 })
        .skip(
          info.page
            ? info.page * info.numberOfElements - info.numberOfElements
            : 0
        )
        .limit(info.numberOfElements);

      res.json({
        success: true,
        message: "Successfully locked lobby.",
        rounds: results,
        numberOfPages: Math.floor(
          (await GameLogModel.find({ lid: ObjectId(info.lid) }).count()) /
            info.numberOfElements
        )
      });
    });
  })
  .post(auth.checkJWT, (req, res, next) => {
    LobbyModel.findOne({ _id: req.params.lid }, async (err, lobby) => {
      if (err) {
        res
          .status(500)
          .json({ success: false, message: "Error querying for lobby" });
        return;
      }

      if (!lobby) {
        res.status(404).json({
          success: false,
          message: "No lobby found."
        });
        return;
      }

      // TODO!
    });
  });

router.post("/lock-lobby/:lid", auth.checkJWT, (req, res, next) => {
  if (req.body.isLocked == undefined || typeof req.body.isLocked != "boolean") {
    res.status(422).json({
      success: false,
      message: "Bad input."
    });
    return;
  }

  LobbyModel.findOne({ _id: req.params.lid }, (err, lobby) => {
    if (err) {
      res.status(500).json({
        success: false,
        message: "Error querying for lobby."
      });
      return;
    }

    if (!lobby) {
      res.status(404).json({
        success: false,
        message: "Could not find lobby."
      });
      return;
    }

    if (lobby.creator != res.decoded.user._id) {
      res.status(401).json({
        success: true,
        message: "Unauthorized to perform such action."
      });
      return;
    }

    lobby.isLocked = req.body.isLocked;

    TableModel.updateMany(
      { lid: req.params.lid },
      { $set: { isLocked: req.body.isLocked } },
      (err, tables) => {
        if (err) {
          res.status(500).json({
            success: false,
            message: "Error querying for tables."
          });
          return;
        }

        res.json({
          success: true,
          message: "Successfully locked the lobby."
        });
      }
    );
  });
});

module.exports = router;

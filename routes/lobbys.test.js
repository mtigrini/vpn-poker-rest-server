const request = require("supertest");
const mongoose = require("mongoose");
const UserModel = require("../models/user");
const LobbyModel = require("../models/lobby");
const ObjectId = require("mongoose").Types.ObjectId;
const nodemailer = require("nodemailer");
const LOBBY_PERMISSIONS = require("../enums/lobby.permissions");
const JOIN_LOBBY_STATUS = require("../enums/join.lobby.status");

const app = require("../app");

async function createUser(user) {
  await request(app)
    .post(`/api/signup`)
    .send({ ...user })
    .then(async () => {
      const usr = await UserModel.findOne({ username: user.username });
      return request(app).post(
        `/api/confirm-registration/${usr.confirmationToken}`
      );
    });
}

async function loginUser({ username, password }) {
  const response = await request(app)
    .post("/api/login")
    .send({
      usernameOrEmail: username,
      password: password
    });

  return response.body.token;
}

async function askToJoinLobby(lobbyId, memberToken) {
  await request(app)
    .post(`/api/join-lobby/${lobbyId}`)
    .set({ authorization: memberToken });
}

async function acceptJoinLobbyRequest(lobbyId, user, adminToken) {
  const lobby = await LobbyModel.findOne({ _id: lobbyId });
  let data = {};

  // Obliger de convertif a string si c<est un objectid...
  let jlr = lobby.joinLobbyRequests.find(r => r.uid == user._id.toString());

  if (!jlr) {
    return;
  }

  data = {
    jlrid: jlr._id,
    verdict: JOIN_LOBBY_STATUS.APPROVED
  };

  // Accepting the user in the lobby
  await request(app)
    .post(`/api/decide-lobby-join-request/${lobbyId}`)
    .set({ authorization: adminToken })
    .send({ ...data });
}

async function createLobby(adminToken, name) {
  const response = await request(app)
    .post(`/api/create-lobby`)
    .set({ authorization: adminToken })
    .send({
      name: name
    });
  const l = await LobbyModel.findOne({ name: name });
  return response.body.lobby;
}

describe("Testing lobbys endpoints", () => {
  beforeAll(async () => {
    await mongoose.connect(
      process.env.database,
      { useNewUrlParser: true },
      err => {
        if (err) {
          process.exit(1);
        }
      }
    );
  });

  afterAll(async () => {
    await UserModel.deleteMany();
    await LobbyModel.deleteMany();

    // Closing the connection to the database after all tests executed.
    await mongoose.connection.close();
  });

  describe("GET /api/lobbys", () => {
    let token = undefined;
    beforeAll(async () => {
      // mock nodemaileer so that signup asses
      jest.spyOn(nodemailer, "createTransport").mockReturnValue({
        sendMail: jest.fn((message, callback) => {
          callback(null, "info");
        })
      });

      await request(app)
        .post(`/api/signup`)
        .send({
          username: "test",
          email: "test@gmail.com",
          password: "test"
        })
        .then(async response => {
          const user = await UserModel.findOne({ username: "test" });
          return request(app).post(
            `/api/confirm-registration/${user.confirmationToken}`
          );
        })
        .then(async () => {
          return request(app)
            .post("/api/login")
            .send({
              usernameOrEmail: "test",
              password: "test"
            });
        })
        .then(response => {
          token = response.body.token;
        });
    });

    afterAll(async () => {
      await UserModel.deleteMany();
      await LobbyModel.deleteMany();
    });

    test("Should return 200", async done => {
      request(app)
        .get(`/api/lobbys`)
        .set({ authorization: token })
        .then(response => {
          expect(response.statusCode).toBe(200);
          expect(response.body.lobbys.length).toBe(0);
          done();
        });
    });
  });

  describe("POST /api/create-lobby", () => {
    beforeAll(async () => {});
    let token = undefined;
    let user = undefined;
    beforeAll(async () => {
      // mock nodemaileer so that signup asses
      jest.spyOn(nodemailer, "createTransport").mockReturnValue({
        sendMail: jest.fn((message, callback) => {
          callback(null, "info");
        })
      });

      await request(app)
        .post(`/api/signup`)
        .send({
          username: "test",
          email: "test@gmail.com",
          password: "test"
        })
        .then(async response => {
          user = await UserModel.findOne({ username: "test" });
          return request(app).post(
            `/api/confirm-registration/${user.confirmationToken}`
          );
        })
        .then(async () => {
          return request(app)
            .post("/api/login")
            .send({
              usernameOrEmail: "test",
              password: "test"
            });
        })
        .then(response => {
          token = response.body.token;
        });
    });

    afterAll(async () => {
      await LobbyModel.deleteMany();
      await UserModel.deleteMany();
    });

    afterEach(async () => {
      await LobbyModel.deleteMany();
    });

    test("Should return 422", async done => {
      request(app)
        .post(`/api/create-lobby`)
        .set({ authorization: token })
        .send({
          test: "test"
        })
        .then(response => {
          expect(response.statusCode).toBe(422);
          done();
        });
    });

    test("Should return 401", async done => {
      // create 20 lobbys
      for (let i = 0; i < 20; i++) {
        await request(app)
          .post(`/api/create-lobby`)
          .set({ authorization: token })
          .send({
            name: `test${i}`
          });
      }
      request(app)
        .post(`/api/create-lobby`)
        .set({ authorization: token })
        .send({
          name: "test20"
        })
        .then(response => {
          expect(response.statusCode).toBe(401);
          done();
        });
    });

    test("Should return 200", async done => {
      request(app)
        .post(`/api/create-lobby`)
        .set({ authorization: token })
        .send({
          name: "test20"
        })
        .then(response => {
          expect(response.statusCode).toBe(200);
          done();
        });
    });
  });

  describe("GET /lobby/:id", () => {
    let token = undefined;
    beforeAll(async () => {
      // mock nodemaileer so that signup asses
      jest.spyOn(nodemailer, "createTransport").mockReturnValue({
        sendMail: jest.fn((message, callback) => {
          callback(null, "info");
        })
      });

      await request(app)
        .post(`/api/signup`)
        .send({
          username: "test",
          email: "test@gmail.com",
          password: "test"
        })
        .then(async response => {
          const user = await UserModel.findOne({ username: "test" });
          return request(app).post(
            `/api/confirm-registration/${user.confirmationToken}`
          );
        })
        .then(async () => {
          return request(app)
            .post("/api/login")
            .send({
              usernameOrEmail: "test",
              password: "test"
            });
        })
        .then(response => {
          token = response.body.token;
        });
    });

    afterAll(async () => {
      await UserModel.deleteMany();
      await LobbyModel.deleteMany();
    });

    afterEach(async () => {
      await LobbyModel.deleteMany();
    });

    test("Should return 500", async done => {
      request(app)
        .get(`/api/lobby/abc`)
        .set({ authorization: token })
        .then(response => {
          expect(response.statusCode).toBe(500);
          done();
        });
    });

    test("Should return 404", async done => {
      request(app)
        .get(`/api/lobby/${new ObjectId()}`)
        .set({ authorization: token })
        .then(response => {
          expect(response.statusCode).toBe(404);
          done();
        });
    });

    test("Should return 200 and the lobby for the admin", async done => {
      await request(app)
        .post(`/api/create-lobby`)
        .set({ authorization: token })
        .send({
          name: "test20"
        });

      const lobby = await LobbyModel.findOne({ name: "test20" });

      request(app)
        .get(`/api/lobby/${lobby._id}`)
        .set({ authorization: token })
        .then(response => {
          expect(response.statusCode).toBe(200);
          expect(response.body.lobby.permissions).toBe(LOBBY_PERMISSIONS.OWNER);
          done();
        });
    });

    // Receive lobby as a member
    test("Retrieving lobby as a member - Should return 200", async done => {
      const adminToken = token; // FIXME: Repair later
      const createdLobby = await createLobby(adminToken, "test");
      const lobbyMember = {
        username: "bobby",
        email: "bobby@gmail.com",
        password: "bobby"
      };
      await createUser(lobbyMember);
      const userToken = await loginUser(lobbyMember);
      await askToJoinLobby(createdLobby._id, userToken);
      const userMember = await UserModel.findOne({
        username: lobbyMember.username
      });
      await acceptJoinLobbyRequest(createdLobby._id, userMember, adminToken);

      request(app)
        .get(`/api/lobby/${createdLobby._id}`)
        .set({ authorization: userToken })
        .then(response => {
          expect(response.statusCode).toBe(200);
          expect(response.body.lobby.permissions).toBe(
            LOBBY_PERMISSIONS.MEMBER
          );
          done();
        });
    });
  });

  describe("POST join-lobby", () => {
    let adminToken = undefined;
    let memberToken = undefined;
    let createdLobby = undefined;
    beforeAll(async () => {
      // mock nodemaileer so that signup asses
      jest.spyOn(nodemailer, "createTransport").mockReturnValue({
        sendMail: jest.fn((message, callback) => {
          callback(null, "info");
        })
      });

      // Create admin, admin connects and creates lobby
      await request(app)
        .post(`/api/signup`)
        .send({
          username: "test",
          email: "test@gmail.com",
          password: "test"
        })
        .then(async response => {
          const user = await UserModel.findOne({ username: "test" });
          return request(app).post(
            `/api/confirm-registration/${user.confirmationToken}`
          );
        })
        .then(async () => {
          return request(app)
            .post("/api/login")
            .send({
              usernameOrEmail: "test",
              password: "test"
            });
        })
        .then(response => {
          // create lobby
          adminToken = response.body.token;
          return request(app)
            .post(`/api/create-lobby`)
            .set({ authorization: adminToken })
            .send({
              name: "test20"
            });
        })
        .then(response => {
          createdLobby = response.body.lobby;
        });

      // user connects
      await request(app)
        .post(`/api/signup`)
        .send({
          username: "test2",
          email: "test2@gmail.com",
          password: "test2"
        })
        .then(async response => {
          const user = await UserModel.findOne({ username: "test2" });
          return request(app).post(
            `/api/confirm-registration/${user.confirmationToken}`
          );
        })
        .then(async () => {
          return request(app)
            .post("/api/login")
            .send({
              usernameOrEmail: "test2",
              password: "test2"
            });
        })
        .then(response => {
          memberToken = response.body.token;
        });
    });

    afterAll(async () => {
      await UserModel.deleteMany();
      await LobbyModel.deleteMany();
    });

    test("Should return 500", async done => {
      request(app)
        .post(`/api/join-lobby/abc`)
        .set({ authorization: memberToken })
        .then(response => {
          expect(response.statusCode).toBe(500);
          done();
        });
    });

    test("Should return 404", async done => {
      request(app)
        .post(`/api/join-lobby/${new ObjectId()}`)
        .set({ authorization: memberToken })
        .then(response => {
          expect(response.statusCode).toBe(404);
          done();
        });
    });

    test("Should return 509", async done => {
      request(app)
        .post(`/api/join-lobby/${createdLobby._id}`)
        .set({ authorization: adminToken })
        .then(response => {
          expect(response.statusCode).toBe(509);
          done();
        });
    });

    test("Should return 200", async done => {
      request(app)
        .post(`/api/join-lobby/${createdLobby._id}`)
        .set({ authorization: memberToken })
        .then(response => {
          expect(response.statusCode).toBe(200);
          done();
        });
    });

    test("Should return 509 cause already member of the lobby", async done => {
      request(app)
        .post(`/api/join-lobby/${createdLobby._id}`)
        .set({ authorization: memberToken })
        .then(response => {
          expect(response.statusCode).toBe(509);
          done();
        });
    });

    test("Should return 509 because member was removed fron lobby", async done => {
      // Create a user so that the user can access the lobby

      // Creating user not apart of lobby
      let newUserToken = undefined;
      // Creating a user that is not an admin of the lobby nor a member and trying to get
      // the lobby
      await request(app)
        .post(`/api/signup`)
        .send({
          username: "test5",
          email: "test5@gmail.com",
          password: "test5"
        });

      const newUser = await UserModel.findOne({ username: "test5" });

      // Confirming user, logging in and getting token, asking to join the lobby
      await request(app)
        .post(`/api/confirm-registration/${newUser.confirmationToken}`)
        .then(response => {
          return request(app)
            .post("/api/login")
            .send({
              usernameOrEmail: "test5",
              password: "test5"
            });
        })
        .then(response => {
          newUserToken = response.body.token;
          return request(app)
            .post(`/api/join-lobby/${createdLobby._id}`)
            .set({ authorization: newUserToken });
        });

      const lobby = await LobbyModel.findOne({ _id: createdLobby._id });
      const data = {
        jlrid: lobby.joinLobbyRequests[0]._id,
        verdict: JOIN_LOBBY_STATUS.APPROVED
      };

      // Accepting the user in the lobby
      await request(app)
        .post(`/api/decide-lobby-join-request/${createdLobby._id}`)
        .set({ authorization: adminToken })
        .send({ ...data });

      // Removing the user from the lobby
      await request(app)
        .post(`/api/remove-from-lobby/${createdLobby._id}`)
        .send({
          memberId: newUser._id
        })
        .set({ authorization: adminToken });

      request(app)
        .post(`/api/join-lobby/${createdLobby._id}`)
        .set({ authorization: newUserToken })
        .then(response => {
          expect(response.statusCode).toBe(509);
          done();
        });
    });
  });

  describe("POST remove-from-lobby/:lid", () => {
    let adminToken = undefined;
    beforeAll(async () => {
      const admin = {
        username: "test",
        email: "test@gmail.com",
        password: "test"
      };
      await createUser(admin);
      adminToken = await loginUser(admin);
    });
    afterAll(async () => {
      await UserModel.deleteMany();
    });

    test("Should return 500 for bad user id", async done => {
      request(app)
        .post(`/api/remove/abc/from-lobby/${new ObjectId()}`)
        .send({ test: "test" })
        .set({ authorization: adminToken })
        .then(response => {
          expect(response.statusCode).toBe(500);
          done();
        });
    });

    test("Should return 404 for inexistant user", async done => {
      request(app)
        .post(`/api/remove/${new ObjectId()}/from-lobby/${new ObjectId()}`)
        .send({ test: "test" })
        .set({ authorization: adminToken })
        .then(response => {
          expect(response.body.message).toBe("User not found.");
          expect(response.statusCode).toBe(404);
          done();
        });
    });

    test("Should return 500 for bas lobby id", async done => {
      const randomUser = {
        username: "test2",
        email: "test2@gmail.com",
        password: "test2"
      };

      await createUser(randomUser);
      const token = await loginUser(randomUser);
      const randomUserObject = await UserModel.findOne({
        username: randomUser.username
      });

      request(app)
        .post(`/api/remove/${randomUserObject._id}/from-lobby/abc`)
        .send({ test: "test" })
        .set({ authorization: token })
        .then(response => {
          expect(response.statusCode).toBe(500);
          done();
        });
    });

    test("Should return 404 for lobby not found", async done => {
      const randomUser = {
        username: "test2",
        email: "test2@gmail.com",
        password: "test2"
      };

      await createUser(randomUser);
      const token = await loginUser(randomUser);
      const randomUserObject = await UserModel.findOne({
        username: randomUser.username
      });

      request(app)
        .post(
          `/api/remove/${randomUserObject._id}/from-lobby/${new ObjectId()}`
        )
        .send({ test: "test" })
        .set({ authorization: token })
        .then(response => {
          expect(response.statusCode).toBe(404);
          done();
        });
    });
  });

  describe("POST applicate-for-chips", () => {
    // TODO: Faire les differents cas de tests pour ca
  });

  describe("POST decide-lobby-join-request", () => {
    // TODO: Faire les differents cas de tests pour ca
  });

  describe("POST decide-lobby-chip-appliation-request", () => {
    // TODO: Faire les differents cas de tests pour ca
  });
});

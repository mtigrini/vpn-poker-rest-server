const router = require("express").Router();
const LobbyModel = require("../models/lobby");
const UserModel = require("../models/user");
const ChipApplicationModel = require("../models/chip_application");

const CHIP_APPLICATION_STATUS = require("../enums/chips.application.status");
const auth = require("../middlewares/check-jwt");

router.post("/applicate-for-chips/:lid", auth.checkJWT, (req, res, next) => {
  LobbyModel.findOne({ _id: req.params.lid }, (err, lobby) => {
    if (err) {
      res.status(500).json({
        success: false,
        message: "Error querying for lobby."
      });
      return;
    }

    if (!lobby) {
      res.status(404).json({
        success: false,
        message: "Lobby not found."
      });
      return;
    }

    let member = lobby.members.find(m => m.uid == req.decoded.user._id);
    if (!member && lobby.creator != req.decoded.user._id) {
      res.status(403).json({
        success: false,
        message: "This action is unauthorized"
      });
      return;
    }

    let chipApplication = new ChipApplicationModel();
    chipApplication.applicantUid = req.decoded.user._id;
    chipApplication.beneficiaryUid = req.decoded.user._id;
    chipApplication.lid = req.params.lid;
    chipApplication.username = req.decoded.user.username;
    chipApplication.amount = req.body.chipApplicationObj.amount;
    chipApplication.status = CHIP_APPLICATION_STATUS.PENDING;

    chipApplication.save(function(error) {
      if (err) {
        res.status(500).json({
          success: false,
          message:
            "Error updating the lobby. Your chip application was not properly treated"
        });
        return;
      }

      res.json({
        code: 200,
        success: true,
        message: "Successfully updated the lobby."
      });
    });
  });
});

router.post(
  "/decide-lobby-chip-application-request/:lid",
  auth.checkJWT,
  (req, res, next) => {
    if (
      !req.body.verdict ||
      (req.body.verdict != CHIP_APPLICATION_STATUS.APPROVED &&
        req.body.verdict != CHIP_APPLICATION_STATUS.DECLINED)
    ) {
      res.status(422).json({
        success: false,
        message: "Bad input. Bad verdict."
      });
      return;
    }

    if (!req.body.caid) {
      res.status(422).json({
        success: false,
        message: "Bad input. caid missing."
      });
      return;
    }

    LobbyModel.findOne(
      {
        _id: req.params.lid
      },
      (err, lobby) => {
        if (err) {
          res.status(500).json({
            success: false,
            message: "Error querying for lobby."
          });
          return;
        }

        if (!lobby) {
          res.status(404).json({
            success: false,
            message: "Lobby not found."
          });
          return;
        }

        if (lobby.creator != req.decoded.user._id) {
          res.status(401).json({
            success: false,
            message:
              "Unauthorized to do this action. You are not the lobby admin."
          });
          return;
        }

        ChipApplicationModel.findOne(
          { _id: req.body.caid },
          (err, chipApplication) => {
            if (err) {
              res.status(500).json({
                success: false,
                message: "Error querying for chip application."
              });
              return;
            }

            if (chipApplication.status != CHIP_APPLICATION_STATUS.PENDING) {
              res.status(409).json({
                success: false,
                message: "Chip application already processed."
              });
              return;
            }

            chipApplication.status = req.body.verdict;
            chipApplication.decided = Date.now();
            const applicantUid = chipApplication.applicantUid;

            chipApplication.save(function(err) {
              if (err) {
                res.status(500).json({
                  success: false,
                  message: "Failed to save chip application."
                });
                return;
              }

              if (req.body.verdict == CHIP_APPLICATION_STATUS.APPROVED) {
                UserModel.findOne({ _id: applicantUid }, (err, user) => {
                  if (err) {
                    chipApplication.status = CHIP_APPLICATION_STATUS.PENDING;
                    chipApplication.save();
                    res.status(500).json({
                      success: false,
                      message: "Error querying for user."
                    });
                    return;
                  }

                  if (!user) {
                    chipApplication.status = CHIP_APPLICATION_STATUS.PENDING;
                    chipApplication.save();
                    res.status(404).json({
                      success: false,
                      message: "User not found."
                    });
                    return;
                  }

                  let chips = user.lobbyChips.find(
                    c => c.lid == req.params.lid
                  );

                  var index = user.lobbyChips.indexOf(chips);

                  chips.amount += chipApplication.amount;

                  user.lobbyChips[index] = chips;

                  user.save(function(err) {
                    if (err) {
                      chipApplication.status = CHIP_APPLICATION_STATUS.PENDING;
                      chipApplication.save();
                      res.status(500).json({
                        success: false,
                        message: "Error updating the user"
                      });
                      return;
                    }

                    res.json({
                      success: true,
                      message: "Successfully treated the chip application."
                    });
                    return;
                  });
                });
              } else {
                res.json({
                  success: true,
                  message: "Successfully treated the chip application."
                });
              }
            });
          }
        );
      }
    );
  }
);

router.post(
  "/lobby/:lid/add-remove-funds/:uid",
  auth.checkJWT,
  (req, res, next) => {
    if (
      !req.body.amount ||
      typeof req.body.amount != "number" ||
      req.body.amount == 0
    ) {
      res.status(422).json({
        success: false,
        message: "Bad input."
      });
      return;
    }

    LobbyModel.findOne({ _id: req.params.lid }, (err, lobby) => {
      if (err) {
        res.status(500).json({
          success: false,
          message: "Error querying for lobby."
        });
        return;
      }

      if (!lobby) {
        res.status(404).json({
          success: false,
          message: "Lobby not found."
        });
        return;
      }

      if (lobby.creator != req.decoded.user._id) {
        res.status(403).json({
          success: false,
          message: "You are unauthorized to perform this action."
        });
        return;
      }

      if (!lobby.members.find(m => m.uid == req.params.uid)) {
        res.status(404).json({
          success: false,
          message: "User not apart of lobby."
        });
        return;
      }

      let chipApplication = new ChipApplicationModel();
      chipApplication.applicantUid = req.decoded.user._id;
      chipApplication.beneficiaryUid = req.params.uid;
      chipApplication.lid = req.params.lid;
      chipApplication.username = req.decoded.user.username;
      chipApplication.amount = Math.round(req.body.amount);
      chipApplication.status = CHIP_APPLICATION_STATUS.APPROVED;
      chipApplication.decided = Date.now();

      chipApplication.save(function(err, savedChipApplication) {
        if (err) {
          res.status(500).json({
            success: false,
            message: "An error occured when creating the fund request."
          });
          return;
        }

        UserModel.findOne({ _id: req.params.uid }, async (err, user) => {
          if (err) {
            // TODO: What happens if the thing does not work.
            await ChipApplicationModel.deleteOne({
              _id: savedChipApplication._id
            });
            res.status(500).json({
              success: false,
              message: "Error occured finding the user."
            });
            return;
          }

          if (!user) {
            await ChipApplicationModel.deleteOne({
              _id: savedChipApplication._id
            });
            res.status(404).json({
              success: false,
              message: "User not found."
            });
            return;
          }

          user.lobbyChips.find(lc => {
            if (lc.lid == req.params.lid) {
              lc.amount += Math.round(req.body.amount);
            }
            return lc;
          });

          user.save(async function(err, savedChipApplication) {
            if (err) {
              // TODO: What happens if the thing does not work.
              await ChipApplicationModel.deleteOne({
                _id: savedChipApplication._id
              });
              res.status(500).json({
                success: false,
                message: "Error occured saving user."
              });
              return;
            }

            res.json({
              success: true,
              message: "Successfully added funds to the user.",
              chipApplication: savedChipApplication
            });
          });
        });
      });
    });
  }
);

router.get("/lobby/:lid/funds-requests", auth.checkJWT, (req, res, next) => {
  LobbyModel.findOne({ _id: req.params.lid }, (err, lobby) => {
    if (err) {
      res.status(500).json({
        success: false,
        message: "Error querying for lobby."
      });
      return;
    }

    if (!lobby) {
      res.status(404).json({
        success: false,
        message: "Lobby not found."
      });
      return;
    }

    if (lobby.creator != req.decoded.user._id) {
      res.status(403).json({
        success: false,
        message: "You are unauthorized to view this information."
      });
      return;
    }

    const statuses = req.query.status
      ? req.query.status.includes(",")
        ? req.query.status.split(",")
        : [req.query.status]
      : [];

    const params = statuses.length > 0 ? { status: { $in: statuses } } : {};

    ChipApplicationModel.find(params, (err, chipApplications) => {
      if (err) {
        res.status(500).json({
          success: false,
          message: "Error querying for chip applications."
        });
        return;
      }

      res.json({
        success: true,
        message: "Successfully retrieved chip applications.",
        chipApplications: chipApplications
      });
    });
  });
});

router.get(
  "/lobby/:lid/funds-requests/:uid",
  auth.checkJWT,
  (req, res, next) => {
    LobbyModel.findOne({ _id: req.params.lid }, (err, lobby) => {
      if (err) {
        res.status(500).json({
          success: false,
          message: "Error querying for lobby."
        });
        return;
      }

      if (!lobby) {
        res.status(404).json({
          success: false,
          message: "Lobby not found."
        });
        return;
      }

      if (lobby.creator != req.decoded.user._id) {
        res.status(403).json({
          success: false,
          message: "You are unauthorized to view this information."
        });
        return;
      }

      if (!lobby.members.find(m => m.uid == req.params.uid)) {
        res.status(404).json({
          success: false,
          message: "User not member of the lobby."
        });
        return;
      }

      ChipApplicationModel.find({
        $and: [
          { beneficiaryUid: req.params.uid },
          {
            status: {
              $in: [
                CHIP_APPLICATION_STATUS.APPROVED,
                CHIP_APPLICATION_STATUS.DECLINED
              ]
            }
          }
        ]
      })
        .sort({ decided: -1 })
        .exec((err, chipApplications) => {
          if (err) {
            res.status(500).json({
              success: false,
              message: "Error querying for chip applications."
            });
            return;
          }

          res.json({
            success: true,
            message: "Successfully retrieved chip applications.",
            chipApplications: chipApplications
          });
        });
    });
  }
);

module.exports = router;

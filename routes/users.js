const jwt = require("jsonwebtoken");
const router = require("express").Router();
const auth = require("../middlewares/check-jwt");
const nodemailer = require("nodemailer");
const generateToken = require("../utils/confirmation-token");
const UserModel = require("../models/user");
const { google } = require("googleapis");

router.post("/signup", async (req, res, next) => {
  if (!req.body.username || !req.body.password || !req.body.email) {
    res.status(422).json({
      success: false,
      message: "Bad input."
    });
    return;
  }

  let user = new UserModel();
  user.username = req.body.username;
  user.password = req.body.password;
  user.email = req.body.email;
  let confirmationToken = undefined;

  // To avoid collision for the confirmation token
  do {
    confirmationToken = generateToken();
  } while (await UserModel.findOne({ confirmationToken: confirmationToken })); // if == null, we get out of the loop

  user.confirmationToken = confirmationToken;

  let avatar = (Math.floor(Math.random() * 47) + 1).toString() + ".png";
  user.avatar = avatar;

  UserModel.findOne(
    { $or: [{ username: req.body.username }, { email: req.body.email }] },
    async (err, existingUser) => {
      if (err) {
        res.status(500).json({
          success: false,
          message: "Error occured while querying for user.",
          err: err
        });
        return;
      }

      if (existingUser) {
        res.status(401).json({
          success: false,
          message: "Account with that email or username already exists."
        });
        return;
      }

      user.save(async function(err, savedUser) {
        if (err) {
          res.status(500).json({
            success: false,
            message: "Error saving the user."
          });
          return;
        }

        const message = {
          from: process.env.email, // Sender address
          to: user.email, // List of recipients, could be an array
          subject: "No Reply : Confirm registration", // Subject line
          html:
            "<h2> Time to confirm your email!<h2>" +
            "<h4> Simply click on this <a href='" +
            process.env.frontendurl +
            "/confirm-registration?token=" +
            user.confirmationToken +
            "'>link</a>.</h4>"
        };

        let transporter = nodemailer.createTransport({
          host: "smtp-mail.outlook.com", // hostname
          secureConnection: false, // TLS requires secureConnection to be false
          port: 587, // port for secure SMTP
          auth: {
              user:  process.env.email,
              pass: process.env.EMAIL_PASS
          },
          tls: {
              ciphers:'SSLv3'
          }
        });

        transporter.sendMail(message, async function(err, info) {
          if (err) {
            await UserModel.deleteOne({_id: savedUser._id});
            res.status(500).json({
              message:
                "There was an error sending the confirmation mail. Please contact support.",
              error: err
            });
            return;
          }

          res.json({
            success: true,
            message: "An email has been sent to confirm your registration."
          });
        });
      });
    }
  );
});

router.post("/login", (req, res, next) => {
  if (req.headers.authorization) {
    const token = req.headers.authorization;
    jwt.verify(token, process.env.jwtsecret, function(err, decoded) {
      if (err) {
        if (err["name"] == "TokenExpiredError") {
          res.status(403).json({
            success: false,
            message: "Token expired. Log in again with your credentials."
          });
        } else {
          res.status(401).json({
            success: false,
            message: "Failed to authenticate"
          });
        }
        return;
      }

      req.decoded = decoded;

      res.json({
        success: true,
        message: "Successfully logged in.",
        user: {
          _id: req.decoded.user._id,
          username: req.decoded.user.username,
          avatar: req.decoded.user.avatar,
          created: req.decoded.user.created
        },
        token: token
      });
      return;
    });
    return;
  }
  // If no token provided (only credentials)
  if (!req.body.usernameOrEmail || !req.body.password) {
    res.status(422).json({
      success: false,
      message: "Bad input."
    });
    return;
  }

  UserModel.findOne(
    {
      $or: [
        { username: req.body.usernameOrEmail },
        { email: req.body.usernameOrEmail }
      ]
    },
    (err, user) => {
      if (err) {
        res.status(500).json({
          success: false,
          message: "Error trying to log in."
        });
        return;
      }

      if (!user) {
        res.status(404).json({
          success: false,
          message: "Authentification failed, user not found."
        });
        return;
      }

      if (!user.isAccountConfirmed) {
        res.status(401).json({
          success: false,
          message:
            "Account is not confirmed. Confirm it first before logging in."
        });
        return;
      }

      if (user) {
        if (!user.comparePasswords(req.body.password)) {
          res.status(401).json({
            success: false,
            message: "Authentication failed. Wrong username or password."
          });
        } else {
          var token = jwt.sign(
            {
              user: {
                _id: user._id,
                username: user.username,
                avatar: user.avatar,
                created: user.created
              }
            },
            process.env.jwtsecret,
            {
              expiresIn: "7d"
            }
          );

          res.json({
            success: true,
            message: "Successfully logged in.",
            user: {
              _id: user._id,
              username: user.username,
              avatar: user.avatar,
              created: user.created
            },
            token: token
          });
        }
      }
    }
  );
});

router.post("/resend-confirmation-mail/:userEmail", (req, res, next) => {
  UserModel.findOne({ email: req.params.userEmail }, async (err, user) => {
    if (err) {
      res.status(500).json({
        success: false,
        message: "Error querying for user."
      });
      return;
    }

    if (!user || user.isAccountConfirmed) {
      res.json({
        success: false,
        message: "An error occured."
      });
      return;
    }

    const message = {
      from: process.env.email, // Sender address
      to: user.email, // List of recipients, could be an array
      subject: "No Reply : Confirm registration", // Subject line
      html:
        "<h2> Time to confirm your email!<h2>" +
        "<h4> Simply click on this <a href='" +
        process.env.frontendurl +
        "/confirm-registration?token=" +
        user.confirmationToken +
        "'>link</a>.</h4>"
    };

    let transporter = nodemailer.createTransport({
      host: "smtp-mail.outlook.com", // hostname
      secureConnection: false, // TLS requires secureConnection to be false
      port: 587, // port for secure SMTP
      auth: {
          user: process.env.email,
          pass: process.env.EMAIL_PASS
      },
      tls: {
          ciphers:'SSLv3'
      }
    });

    transporter.sendMail(message, function(err, info) {
      if (err) {
        res.status(500).json({
          message:
            "There was an error sending the confirmation mail. Please contact support.",
          error: err
        });
        return;
      }

      res.json({
        success: true,
        message: "An email has been sent to confirm your registration."
      });
    });
  });
});

// route for simply getting a user
router.get("/user/:id", (req, res, next) => {
  UserModel.findOne(
    { _id: req.params.id },
    { _id: 1, username: 1, avatar: 1, created: 1 },
    (err, user) => {
      if (err) {
        res.status(500).json({
          success: false,
          message: "Failed to query for user."
        });
        return;
      }

      if (!user) {
        res.status(404).json({
          success: false,
          message: "User not found."
        });
        return;
      }

      res.json({
        success: true,
        message: "Successfully retrieved user.",
        user: user
      });
    }
  );
});

router.post("/confirm-registration/:confirmationToken", (req, res, next) => {
  UserModel.findOne(
    {
      confirmationToken: req.params.confirmationToken
    },
    async (err, existingUser) => {
      if (err) {
        res.status(500).json({
          success: true,
          message: "Error finding user."
        });
        return;
      }

      if (!existingUser) {
        res.status(404).json({
          success: false,
          message: "User not found."
        });
        return;
      }

      if (existingUser.isAccountConfirmed) {
        res.status(409).json({
          success: false,
          message: "User already confirmed."
        });
        return;
      }

      existingUser.isAccountConfirmed = true;

      existingUser.save((err, updatedUser) => {
        if (err) {
          res.status(500).json({
            success: false,
            message: "Error saving user."
          });
          return;
        }

        res.json({
          success: true,
          message: "Successfully confirmed your account. You can now log in."
        });
      });
    }
  );
});

router
  .route("/profile")
  .get(auth.checkJWT, (req, res, next) => {
    UserModel.findOne(
      { _id: req.decoded.user._id },
      { _id: 1, created: 1, username: 1, email: 1, avatar: 1, lobbyChips: 1 },
      (err, user) => {
        if (err) {
          res.status(500).json({
            success: true,
            message: "Error while retrieve user."
          });
          return;
        }

        if (!user) {
          res.status(404).json({
            success: false,
            message: "User not found."
          });
          return;
        }

        res.json({
          success: true,
          message: "Successfully retrieved user.",
          user: user
        });
      }
    );
  })
  .put(auth.checkJWT, (req, res, next) => {
    UserModel.findOne({ _id: req.decoded.user._id }, async (err, user) => {
      if (err) {
        res.status(500).json({
          success: false,
          error: err,
          message: "An error occured while querying for the user."
        });
        return;
      }

      if (req.body.email) {
        if (
          (await UserModel.findOne({ email: req.body.email })) &&
          user.email != req.body.email
        ) {
          res.status(409).json({
            success: false,
            message: "Could not change to this mail."
          });
          return;
        }
        user.email = req.body.email;
      }

      if (req.body.avatar) user.avatar = req.body.avatar;

      if (req.body.newPassword) {
        if (!user.comparePasswords(req.body.currentPassword)) {
          res.status(403).json({
            success: false,
            message: "Failed to edit your profile."
          });
          return;
        } else {
          user.password = req.body.newPassword;
        }
      }

      user.save();

      res.json({
        success: true,
        message: "Successfully edited your profile."
      });
    });
  });

router.route("/reset-password").post((req, res, next) => {
  if (!req.body.email) {
    res.status(422).json({
      success: false,
      message: "Input missing. Email not provided."
    });
    return;
  }

  UserModel.findOne({ email: req.body.email }, async (err, user) => {
    if (err) {
      res.status(500).json({
        success: false,
        message: "An error occured."
      });
      return;
    }

    if (!user) {
      res.status(404).json({
        success: false,
        message: "No user found."
      });
      return;
    }

    if (!user.isAccountConfirmed) {
      res.status(401).json({
        success: false,
        message: "Account not yet confirmed."
      });
      return;
    }

    user.resetPasswordToken = generateToken(); // No need for this to be unique

    user.save(async function(err) {
      if (err) {
        res.status(500).json({
          success: false,
          message: "Error generating token for this user."
        });
        return;
      }

      const oauth2Client = new OAuth2(
        process.env.clientId,
        process.env.clientSecret,
        "https://developers.google.com/oauthplayground" // Redirect URL
      );

      // Sending the confirmation message to the email of the one that created an account.
      const message = {
        from: process.env.email, // Sender address
        to: user.email, // List of recipients
        subject: "No reply : Reset your password", // Subject line
        html:
          "<h2> Reset your password by clicking on this  " +
          "<a href='" +
          process.env.frontendurl +
          "/reset-password?token=" +
          user.resetPasswordToken +
          "'>link" +
          "</a></h4>"
      };
      oauth2Client.setCredentials({
        refresh_token: process.env.refreshToken
      });

      const accessToken = await oauth2Client
        .getAccessToken()
        .then(data => {
          return data.token;
        })
        .catch(err => {
          console.log("COULD NOT GENERATE ACCESS TOKEN : ", err);
        });

      let transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
          type: "OAuth2",
          user: process.env.email,
          clientId: process.env.clientId,
          clientSecret: process.env.clientSecret,
          refreshToken: process.env.refreshToken,
          accessToken: accessToken,
          accessType: "offline"
        }
      });
      transporter.sendMail(message, function(err, info) {
        if (err) {
          res.status(500).json({
            success: false,
            message: "There was an error.",
            error: err
          });
          return;
        }

        res.json({
          success: true,
          message: "An email has been sent to reset your password."
        });
      });
    });
  });
});

router
  .route("/confirm-reset-password/:resetPasswordToken")
  .post((req, res, next) => {
    // We process the reset password demand
    if (!req.body.newPassword || !req.body.confirmNewPassword) {
      res.status(422).json({
        success: false,
        message: "Bad input."
      });
      return;
    }

    if (req.body.newPassword != req.body.confirmNewPassword) {
      res.status(422).json({
        success: false,
        message: "Passwords do not match."
      });
      return;
    }

    UserModel.findOne(
      { resetPasswordToken: req.params.resetPasswordToken },
      (err, user) => {
        if (err) {
          res.status(500).json({
            success: false,
            message: "An error occured."
          });
          return;
        }

        if (!user) {
          res.status(404).json({
            success: false,
            message: "Token not valid."
          });
          return;
        }

        if (!user.isAccountConfirmed) {
          res.status(401).json({
            success: false,
            message:
              "Account not yet confirmed. Click on the link in your email to activate your account."
          });
          return;
        }

        user.password = req.body.newPassword;
        user.resetPasswordToken = null;

        user.save(err => {
          if (err) {
            res.status(500).json({
              success: false,
              message: "Error while saving new password."
            });
            return;
          }

          res.json({
            success: true,
            message: "Successfully reset password. You can connect now log in."
          });
        });
      }
    );
  });

module.exports = router;

const request = require("supertest");
const mongoose = require("mongoose");
const UserModel = require("../models/user");
const LobbyModel = require("../models/lobby");
const TableModel = require("../models/table").tableModel;
const GAME_MODE = require("../enums/game.settings");
const GAME_TYPE = require("../enums/game.type");
const JOIN_LOBBY_STATUS = require("../enums/join.lobby.status");

const app = require("../app");

describe("Testing lobbys endpoints", () => {
  let adminToken = undefined;
  let adminLobby = undefined;
  beforeAll(async () => {
    await mongoose.connect(
      process.env.database,
      { useNewUrlParser: true },
      err => {
        if (err) {
          process.exit(1);
        }
      }
    );

    // Creating a user
    await request(app)
      .post(`/api/signup`)
      .send({
        username: "test",
        email: "test@gmail.com",
        password: "test"
      });

    const user = await UserModel.findOne({ username: "test" });

    // Confirming user, logging in and getting token
    await request(app)
      .post(`/api/confirm-registration/${user.confirmationToken}`)
      .then(response => {
        return request(app)
          .post("/api/login")
          .send({
            usernameOrEmail: "test",
            password: "test"
          });
      })
      .then(response => {
        adminToken = response.body.token;
      });

    // Creating a lobby
    await request(app)
      .post(`/api/create-lobby`)
      .set({ authorization: adminToken })
      .send({
        name: "test20"
      })
      .then(response => {
        adminLobby = response.body.lobby;
      });
  });

  afterAll(async () => {
    // Closing the connection to the database after all tests executed.
    await LobbyModel.deleteMany();
    await UserModel.deleteMany();
    await TableModel.deleteMany();
    await mongoose.connection.close();
  });

  describe("GET /tables/:lid", () => {
    afterAll(async () => {
      await TableModel.deleteMany();
    });

    test("Should return 500", done => {
      request(app)
        .get(`/api/tables/abc`)
        .set({ authorization: adminToken })
        .then(response => {
          expect(response.statusCode).toBe(500);
          done();
        });
    });

    test("Should return 401", async done => {
      // Creating user not apart of lobby
      let newUserToken = undefined;
      // Creating a user that is not an admin of the lobby nor a member and trying to get
      // the lobby
      await request(app)
        .post(`/api/signup`)
        .send({
          username: "test3",
          email: "test3@gmail.com",
          password: "test3"
        });

      const newUser = await UserModel.findOne({ username: "test3" });

      // Confirming user, logging in and getting token
      await request(app)
        .post(`/api/confirm-registration/${newUser.confirmationToken}`)
        .then(() => {
          return request(app)
            .post("/api/login")
            .send({
              usernameOrEmail: "test3",
              password: "test3"
            });
        })
        .then(response => {
          newUserToken = response.body.token;
        });

      request(app)
        .get(`/api/tables/${adminLobby._id}`)
        .set({ authorization: newUserToken })
        .then(response => {
          expect(response.statusCode).toBe(401);
          done();
        });
      done();
    });

    // Getting lobby as an admin
    test("Should return 200", done => {
      request(app)
        .get(`/api/tables/${adminLobby._id}`)
        .set({ authorization: adminToken })
        .then(response => {
          expect(response.statusCode).toBe(200);
          done();
        });
    });

    // Getting table as a removed member of lobby
    test("Should return 401", async done => {
      // Creating user not apart of lobby
      let newUserToken = undefined;
      // Creating a user that is not an admin of the lobby nor a member and trying to get
      // the lobby
      await request(app)
        .post(`/api/signup`)
        .send({
          username: "test5",
          email: "test5@gmail.com",
          password: "test5"
        });

      const newUser = await UserModel.findOne({ username: "test5" });

      // Confirming user, logging in and getting token, asking to join the lobby
      await request(app)
        .post(`/api/confirm-registration/${newUser.confirmationToken}`)
        .then(response => {
          return request(app)
            .post("/api/login")
            .send({
              usernameOrEmail: "test5",
              password: "test5"
            });
        })
        .then(response => {
          newUserToken = response.body.token;
          return request(app)
            .post(`/api/join-lobby/${adminLobby._id}`)
            .set({ authorization: newUserToken });
        });

      const lobby = await LobbyModel.findOne({ _id: adminLobby._id });
      const data = {
        jlrid: lobby.joinLobbyRequests[0]._id,
        verdict: JOIN_LOBBY_STATUS.APPROVED
      };

      // Accepting the user in the lobby
      await request(app)
        .post(`/api/decide-lobby-join-request/${adminLobby._id}`)
        .set({ authorization: adminToken })
        .send({ ...data });

      // Removing the user from the lobby
      await request(app)
        .post(`/api/remove/${newUser._id}/from-lobby/${adminLobby._id}`)
        .set({ authorization: adminToken });

      request(app)
        .get(`/api/tables/${adminLobby._id}`)
        .set({ authorization: newUserToken })
        .then(response => {
          expect(response.statusCode).toBe(401);
          done();
        });
      done();
    });

    // Get table as accepted member
    test("Should return 200", async done => {
      // Creating user not apart of lobby
      let newUserToken = undefined;
      // Creating a user that is not an admin of the lobby nor a member and trying to get
      // the lobby
      await request(app)
        .post(`/api/signup`)
        .send({
          username: "test4",
          email: "test4@gmail.com",
          password: "test4"
        });

      const newUser = await UserModel.findOne({ username: "test4" });

      // Confirming user, logging in and getting token, asking to join the lobby
      await request(app)
        .post(`/api/confirm-registration/${newUser.confirmationToken}`)
        .then(response => {
          return request(app)
            .post("/api/login")
            .send({
              usernameOrEmail: "test4",
              password: "test4"
            });
        })
        .then(response => {
          newUserToken = response.body.token;
          return request(app)
            .post(`/api/join-lobby/${adminLobby._id}`)
            .set({ authorization: newUserToken });
        });

      const lobby = await LobbyModel.findOne({ _id: adminLobby._id });
      const data = {
        jlrid: lobby.joinLobbyRequests[0]._id,
        verdict: JOIN_LOBBY_STATUS.APPROVED
      };

      // Accepting the user in the lobby
      await request(app)
        .post(`/api/decide-lobby-join-request/${adminLobby._id}`)
        .set({ authorization: adminToken })
        .send({ ...data });

      request(app)
        .get(`/api/tables/${adminLobby._id}`)
        .set({ authorization: newUserToken })
        .then(response => {
          expect(response.statusCode).toBe(200);
          done();
        });
      done();
    });
  });

  describe("POST /tables/:lid", () => {
    afterAll(async () => {
      await TableModel.deleteMany();
    });

    test("Shoud return 422", async done => {
      const table = {
        smallBlind: 10,
        bigBlind: 20
      };
      request(app)
        .post(`/api/tables/${adminLobby._id}`)
        .set({ authorization: adminToken })
        .send({
          ...table
        })
        .then(response => {
          expect(response.statusCode).toBe(422);
          done();
        });
    });

    test("Should return 500 because of bad lobby id", async done => {
      let table = {
        smallBlind: 10,
        bigBlind: 20,
        minBuyIn: 100,
        maxBuyIn: 1000,
        minPlayers: 2,
        maxPlayers: 9,
        gameMode: GAME_MODE.NORMAL,
        rake: 10,
        gameType: GAME_TYPE.OMAHA
      };

      request(app)
        .post(`/api/tables/abc`)
        .set({ authorization: adminToken })
        .send({
          ...table
        })
        .then(response => {
          expect(response.statusCode).toBe(500);
          done();
        });
    });

    test("Should return 401 user not apart of lobby", async done => {
      let table = {
        smallBlind: 10,
        bigBlind: 20,
        minBuyIn: 100,
        maxBuyIn: 1000,
        minPlayers: 2,
        maxPlayers: 9,
        gameMode: GAME_MODE.NORMAL,
        rake: 10,
        gameType: GAME_TYPE.OMAHA
      };

      let newUserToken = undefined;
      // Creating a user that is not an admin of the lobby
      await request(app)
        .post(`/api/signup`)
        .send({
          username: "test2",
          email: "test2@gmail.com",
          password: "test2"
        });

      const newUser = await UserModel.findOne({ username: "test2" });

      // Confirming user, logging in and getting token
      await request(app)
        .post(`/api/confirm-registration/${newUser.confirmationToken}`)
        .then(() => {
          return request(app)
            .post("/api/login")
            .send({
              usernameOrEmail: "test2",
              password: "test2"
            });
        })
        .then(response => {
          newUserToken = response.body.token;
        });

      request(app)
        .post(`/api/tables/${adminLobby._id}`)
        .set({ authorization: newUserToken })
        .send({
          ...table
        })
        .then(response => {
          expect(response.statusCode).toBe(401);
          done();
        });
    });

    test("Should return 401 user not apart of lobby", async done => {
      let table = {
        smallBlind: -10,
        bigBlind: 20,
        minBuyIn: 100,
        maxBuyIn: 1000,
        minPlayers: 2,
        maxPlayers: 9,
        gameMode: GAME_MODE.NORMAL,
        rake: 10,
        gameType: GAME_TYPE.OMAHA
      };

      request(app)
        .post(`/api/tables/${adminLobby._id}`)
        .set({ authorization: adminToken })
        .send({
          ...table
        })
        .then(response => {
          expect(response.statusCode).toBe(422);
          expect(response.body.message).toBe(
            "Invalid small blind or big blind."
          );
          done();
        });
    });

    test("Should return 401 user not apart of lobby", async done => {
      let table = {
        smallBlind: 10,
        bigBlind: 20,
        minBuyIn: -10,
        maxBuyIn: 1000,
        minPlayers: 2,
        maxPlayers: 9,
        gameMode: GAME_MODE.NORMAL,
        rake: 10,
        gameType: GAME_TYPE.OMAHA
      };

      request(app)
        .post(`/api/tables/${adminLobby._id}`)
        .set({ authorization: adminToken })
        .send({
          ...table
        })
        .then(response => {
          expect(response.statusCode).toBe(422);
          expect(response.body.message).toBe(
            "Invalid minimum buy in or maximum buy in."
          );
          done();
        });
    });

    test("Should return 401 user not apart of lobby", async done => {
      let table = {
        smallBlind: 10,
        bigBlind: 20,
        minBuyIn: 100,
        maxBuyIn: 1000,
        minPlayers: 1,
        maxPlayers: 10,
        gameMode: GAME_MODE.NORMAL,
        rake: 10,
        gameType: GAME_TYPE.OMAHA
      };

      request(app)
        .post(`/api/tables/${adminLobby._id}`)
        .set({ authorization: adminToken })
        .send({
          ...table
        })
        .then(response => {
          expect(response.statusCode).toBe(422);
          expect(response.body.message).toBe(
            "Invalid minimum players or maximum players."
          );
          done();
        });
    });

    test("Should return 401 user not apart of lobby", async done => {
      let table = {
        smallBlind: 10,
        bigBlind: 20,
        minBuyIn: 100,
        maxBuyIn: 1000,
        minPlayers: 2,
        maxPlayers: 9,
        gameMode: "Bad value",
        rake: 10,
        gameType: GAME_TYPE.OMAHA
      };

      request(app)
        .post(`/api/tables/${adminLobby._id}`)
        .set({ authorization: adminToken })
        .send({
          ...table
        })
        .then(response => {
          expect(response.statusCode).toBe(422);
          expect(response.body.message).toBe("Invalid game mode.");
          done();
        });
    });

    test("Should return 401 user not apart of lobby", async done => {
      let table = {
        smallBlind: 10,
        bigBlind: 20,
        minBuyIn: 100,
        maxBuyIn: 1000,
        minPlayers: 2,
        maxPlayers: 9,
        gameMode: GAME_MODE.NORMAL,
        rake: 35,
        gameType: GAME_TYPE.OMAHA
      };

      request(app)
        .post(`/api/tables/${adminLobby._id}`)
        .set({ authorization: adminToken })
        .send({
          ...table
        })
        .then(response => {
          expect(response.statusCode).toBe(422);
          expect(response.body.message).toBe("Invalide rake.");
          done();
        });
    });

    test("Should return 401 user not apart of lobby", async done => {
      let table = {
        smallBlind: 10,
        bigBlind: 20,
        minBuyIn: 100,
        maxBuyIn: 1000,
        minPlayers: 2,
        maxPlayers: 9,
        gameMode: GAME_MODE.NORMAL,
        rake: 10,
        gameType: "Bad value"
      };

      request(app)
        .post(`/api/tables/${adminLobby._id}`)
        .set({ authorization: adminToken })
        .send({
          ...table
        })
        .then(response => {
          expect(response.statusCode).toBe(422);
          expect(response.body.message).toBe("Invalide game type.");
          done();
        });
    });

    test("Should return 401 user not apart of lobby", async done => {
      let table = {
        smallBlind: 10,
        bigBlind: 20,
        minBuyIn: 100,
        maxBuyIn: 1000,
        minPlayers: 2,
        maxPlayers: 9,
        gameMode: GAME_MODE.NORMAL,
        rake: 10,
        gameType: GAME_TYPE.TXSHLDM
      };

      request(app)
        .post(`/api/tables/${adminLobby._id}`)
        .set({ authorization: adminToken })
        .send({
          ...table
        })
        .then(response => {
          expect(response.statusCode).toBe(200);
          done();
        });
    });
  });
});

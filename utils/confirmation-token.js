module.exports = function() {
  let confirmationToken = "";
  let characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  let charactersLength = characters.length;
  for (var i = 0; i < 20; i++) {
    confirmationToken += characters.charAt(
      Math.floor(Math.random() * charactersLength)
    );
  }

  return confirmationToken;
};

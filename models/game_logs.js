const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const actionLog = new Schema({
  playerId: String,
  action: String,
  hand: Array,
  chips: Number,
  timeStamp: Date
});
// Est-ce que le round id est necessaire?
// La creation de ce shit va anyway creer un id unique.
const game_log = new Schema({
  lid: { type: Schema.Types.ObjectId, required: true },
  tid: { type: Schema.Types.ObjectId, required: true },
  actionLogs: { type: Array },
  board: { type: Array },
  seats: { type: Schema.Types.Mixed },
  start: { type: Date },
  gems: { type: Number },
  end: { type: Date }
});

module.exports = mongoose.model("game_log", game_log, "game_logs");

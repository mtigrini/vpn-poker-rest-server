const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const cashTransactionSchema = new Schema({
  amount: {
    type: Number,
    required: true
  },
  timestamp: {
    type: Date,
    default: Date.now()
  }
});

const transaction = new Schema({
  uid: { type: Schema.Types.ObjectId, required: true },
  tid: { type: Schema.Types.ObjectId, required: true },
  lid: { type: Schema.Types.ObjectId, required: true },
  cashin: {
    type: cashTransactionSchema,
    required: true
  },
  cashout: {
    type: cashTransactionSchema,
    required: false
  }
});

module.exports = mongoose.model("transaction", transaction, "transactions");

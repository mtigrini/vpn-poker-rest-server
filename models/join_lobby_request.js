const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const JOIN_LOBBY_STATUS = require("../enums/join.lobby.status");

const joinLobbyRequest = new Schema({
  uid: { type: Schema.Types.ObjectId },
  lid: { type: Schema.Types.ObjectId, required: true },
  username: String,
  status: {
    type: String,
    default: JOIN_LOBBY_STATUS.PENDING,
    enum: [
      JOIN_LOBBY_STATUS.APPROVED,
      JOIN_LOBBY_STATUS.PENDING,
      JOIN_LOBBY_STATUS.DECLINED
    ]
  },
  created: { type: Date, defauly: Date.now() },
  decided: { type: Date }
});

module.exports = mongoose.model(
  "joinLobbyRequest",
  joinLobbyRequest,
  "join_lobby_requests"
);

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const memberSchema = new Schema({
  uid: { type: Schema.Types.ObjectId }
});

const lobby = new Schema({
  name: { type: String, required: true },
  gems: { type: Number, default: 0 },
  members: [memberSchema],
  removedMembers: [memberSchema],
  creator: { type: Schema.Types.ObjectId, required: true },
  created: { type: Date, default: Date.now() },
  isLocked: { type: Boolean, default: false }
});

module.exports = mongoose.model("lobby", lobby, "lobbys");

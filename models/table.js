const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const GAME_MODE = require("../enums/game.settings");
const GAME_TYPE = require("../enums/game.type");

const table = new Schema({
  // name : {type: string, required: false} -> pourrait peut-etre etre utile de donner un nom a la table.
  lid: { type: Schema.Types.ObjectId, required: true }, // ID of the lobby in wich the table was created.
  smallBlind: { type: Number, required: true, min: 0 },
  bigBlind: { type: Number, required: true, min: 0 },
  minBuyIn: { type: Number, required: true, min: 0 },
  maxBuyIn: { type: Number, required: true, min: 0 },
  minPlayers: { type: Number, min: 0, max: 10 },
  maxPlayers: { type: Number, min: 0, max: 10 },
  gameMode: {
    type: String,
    enum: [GAME_MODE.NORMAL, GAME_MODE.FAST],
    default: GAME_MODE.FAST
  },
  gameType: {
    type: String,
    enum: [GAME_TYPE.TXSHLDM, GAME_TYPE.OMAHA],
    required: true
  },
  creator: { type: Schema.Types.ObjectId, required: true },
  created: { type: Date, default: Date.now() },
  isLocked: { type: Boolean, default: false },
  rake: { type: Number, default: 0, min: 0, max: 30 }
});

module.exports = {
  tableSchema: table,
  tableModel: mongoose.model("table", table, "tables")
};

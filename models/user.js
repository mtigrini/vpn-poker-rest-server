const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const bcrypt = require("bcrypt-nodejs");

const ChipsSchema = new Schema({
  lid: { type: String, required: [true, "The lobby id is required."] },
  amount: {
    type: Number,
    validate: {
      validator: function(v) {
        return v >= 0;
      },
      message: () => "Account balance cannot be negative."
    },
    min: 0
  }
});
// An id is automoatically generated for each user
const UserSchema = new Schema({
  username: { type: String, unique: true },
  password: String,
  email: { type: String, unique: true },
  avatar: String,
  lobbyChips: [ChipsSchema],
  created: { type: Date, default: Date.now() },
  confirmationToken: { type: String, unique: true, required: true },
  isAccountConfirmed: { type: Boolean, default: false },
  resetPasswordToken: { type: String, required: false, default: null }
});

UserSchema.pre("save", function(next) {
  var user = this;

  if (!user.isModified("password")) return next();

  bcrypt.hash(user.password, null, null, function(err, hash) {
    if (err) return next(err);

    user.password = hash;
    next();
  });
});

UserSchema.methods.comparePasswords = function(password) {
  // We simply compare the password that the user typed in and the password in the database
  return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model("user", UserSchema, "users");

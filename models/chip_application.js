const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const CHIP_APPLICATION_STATUS = require("../enums/chips.application.status");

const chipApplication = new Schema({
  applicantUid: { type: Schema.Types.ObjectId },
  beneficiaryUid: { type: Schema.Types.ObjectId },
  lid: { type: Schema.Types.ObjectId, required: true },
  username: String,
  amount: { type: Number },
  status: {
    type: String,
    default: CHIP_APPLICATION_STATUS.PENDING,
    enum: [
      CHIP_APPLICATION_STATUS.APPROVED,
      CHIP_APPLICATION_STATUS.PENDING,
      CHIP_APPLICATION_STATUS.DECLINED
    ]
  },
  created: { type: Date, default: Date.now() },
  decided: { type: Date }
});

module.exports = mongoose.model(
  "chipApplication",
  chipApplication,
  "chip_applications"
);

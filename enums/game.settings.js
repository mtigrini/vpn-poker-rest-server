const GAME_MODE = {
  NORMAL: "normal",
  FAST: "fast"
};

module.exports = GAME_MODE;

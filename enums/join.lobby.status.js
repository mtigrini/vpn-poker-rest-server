const JOIN_LOBBY_STATUS = {
  APPROVED: "Approved",
  DECLINED: "Declined",
  PENDING: "Pending"
};

module.exports = JOIN_LOBBY_STATUS;

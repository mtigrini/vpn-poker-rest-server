const CHIP_APPLICATION_STATUS = {
    APPROVED: "Approved",
    DECLINED: "Declined",
    PENDING:  "Pending"
}

module.exports = CHIP_APPLICATION_STATUS;
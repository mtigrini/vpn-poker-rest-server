const GAME_ROUNDS = {
    DEAL        : "Deal",
    RIVER       : "River",
    TURN        : "Turn",
    FLOP        : "Flop",
    GAME_END    : "GameEnd"
}

module.exports = GAME_ROUNDS;
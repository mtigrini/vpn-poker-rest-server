const GAME_ACTIONS = {
  BET: "bet",
  CALL: "call",
  CHECK: "check",
  ALLIN: "allin",
  FOLD: "fold",
  DEALER: "dealer",
  SMALLBLIND: "small blind", // Not technically an action but will pass
  BIGBLIND: "big blind" // Not technically an action but will pass
};

module.exports = GAME_ACTIONS;

const LOBBY_PERMISSIONS = {
    OWNER: "Owner",
    MEMBER: "Member"
}

module.exports = LOBBY_PERMISSIONS;
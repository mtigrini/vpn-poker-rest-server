const GAME_TYPE = {
  TXSHLDM: "Texas holdem",
  OMAHA: "Omaha"
};

module.exports = GAME_TYPE;

const GAME_STATE = {
    IN_PROGRESS: "IN_PROGRESS",
    JOIN: "JOIN"
}

module.exports = GAME_STATE;